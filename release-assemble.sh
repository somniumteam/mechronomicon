#!/bin/bash

cd "$(dirname "$(readlink -f "$0")")"

OUT_DIR="$PWD/target/deploy-release"

if [[ -d "$OUT_DIR" ]]
then
	rm -r "$OUT_DIR"/*
else
	mkdir -p "$OUT_DIR"
fi

cp "$PWD/target/release/mechronomicon" "$OUT_DIR/"
cp -r assets "$OUT_DIR/assets" || exit $?
rm "$OUT_DIR/assets/index.html" &>/dev/null

cd "$OUT_DIR" || exit $?
7z a mechronomicon-release.zip ./*
exit $?