#![feature(coroutines, coroutine_trait, iter_from_coroutine)]
#![feature(trait_alias)]
#![feature(div_duration)]

use bevy::prelude::*;
use bevy::asset::*;
use bevy_svg::prelude::*;

pub mod util;
pub use util::*;
pub mod hexagons;
pub use hexagons::*;
pub mod ui;
pub use ui::*;
pub mod scenario;
pub use scenario::*;
pub mod terrain;
pub use terrain::*;
pub mod pawns;
pub use pawns::*;
pub mod phases;
pub use phases::*;
pub mod commands;
pub use commands::*;
pub mod players;
pub use players::*;
pub mod dice;
pub use dice::*;
pub mod ai;
pub use ai::*;
pub mod networking;
pub use networking::*;

pub const BACKGROUND_COLOR: Color = Color::Rgba { red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0 };

fn main()
{
	let mut app = App::new();
	app.insert_resource(AssetMetaCheck::Never);
	app.insert_resource(ClearColor(BACKGROUND_COLOR));
	app.add_plugins
	((
		bevy_web_asset::WebAssetPlugin::default(),
		DefaultPlugins.build().set(WindowPlugin
		{
			primary_window: Some(Window
			{
				prevent_default_event_handling: true,
				title: "Mechronomicon".into(),
				resizable: true,
				..default()
			}),
			close_when_requested: true,
			exit_condition: bevy::window::ExitCondition::OnPrimaryClosed,
			..default()
		}),
		SvgPlugin,
		UIPlugin,
		HexagonPlugin
		{
			hex_spacing: 48.0,
			..default()
		},
		TerrainPlugin,
		DicePlugin,
		PawnPlugin,
		PhasesPlugin,
		CommandsPlugin,
		PlayersPlugin,
		AIPlugin,
		ScenarioPlugin,
		NetworkingPlugin,
	));
	app.add_systems(Startup, setup);
	app.run();
}

fn setup
(
	mut commands: Commands,
)
{
	commands.spawn(Camera2dBundle::default());
}