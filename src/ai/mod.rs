use bevy::prelude::*;
use crate::*;

pub struct AIPlugin;
impl Plugin for AIPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<NoopBot>();
		app.add_systems(Update,
		(
			noop_bot_submit_checks,
			noop_bot_submit_plan
				.run_if(in_state(GamePhase::PLANNING))
				.before(plan_submissions),
		));
	}
}

#[derive(Component, Default, Reflect)]
pub struct NoopBot;
#[derive(Bundle)]
pub struct NoopBotBundle
{
	pub scope: ScenarioScope,
	pub ai: NoopBot,
	pub player: PlayerBundle,
}
impl NoopBotBundle
{
	pub fn new<N: Into<Name>>(name: N) -> Self
	{
		Self
		{
			scope: default(),
			ai: default(),
			player: PlayerBundle::new(name),
		}
	}
}

pub fn noop_bot_submit_checks
(
	noop_bots: Query<Entity, With<NoopBot>>,
	mut checks: Query<(Entity, &Check)>,
	mut submissions: EventWriter<SubmitCheckEvent>,
)
{
	for bot in noop_bots.iter()
	{
		for (check_entity, check) in checks.iter_mut()
		{
			if
				check.responsible_players.iter().any(|&p| p == bot)
				&& (check.phase == CheckPhase::NotBoosting || check.phase == CheckPhase::Boosting)
			{
				submissions.send(SubmitCheckEvent
				{
					player: bot,
					check: check_entity,
				});
			}
		}
	}
}

pub fn noop_bot_submit_plan
(
	bots_to_submit: Query
	<
		Entity,
		(
			With<IsPlanning>,
			Without<SubmittedPlan>,
			With<NoopBot>,
		),
	>,
	mut actors: Query<&mut Actor>,
	mut submissions: EventWriter<SubmitPlanEvent>,
)
{
	for mut actor in actors.iter_mut()
	{
		if bots_to_submit.get(actor.controller).is_ok()
		{
			actor.command_queue.push(ActorCommand::new(ActorCommandType::Emote, default()));
		}
	}
	for bot in bots_to_submit.iter()
	{
		submissions.send(SubmitPlanEvent
		{
			player: bot,
		});
	}
}