use bevy::prelude::*;
use bevy_matchbox::prelude::PeerId;

pub mod factions;
pub use factions::*;

pub struct PlayersPlugin;
impl Plugin for PlayersPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<IsLocalPlayer>();
		app.add_plugins(FactionsPlugin);
		app.add_systems(Startup, setup);
	}
}

fn setup
(
	mut commands: Commands,
)
{
	commands.spawn
	((
		PlayerBundle::new(names::Generator::default().next().unwrap()),
		IsLocalPlayer,
	));
}

#[derive(Component, Debug, Clone, Copy, Default)]
pub struct Player
{
	pub peer_id: Option<PeerId>,
}
impl Player
{
	pub fn new() -> Self
	{
		Self
		{
			..default()
		}
	}
}
#[derive(Bundle)]
pub struct PlayerBundle
{
	pub player: Player,
	pub name: Name,
}
impl PlayerBundle
{
	pub fn new<N: Into<Name>>(name: N) -> Self
	{
		Self
		{
			player: Player::new(),
			name: Name::from(name.into()),
		}
	}
}

#[derive(Component, Default, Reflect)]
pub struct IsLocalPlayer;