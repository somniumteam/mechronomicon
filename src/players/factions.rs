use bevy::prelude::*;
use crate::Player;

pub struct FactionsPlugin;
impl Plugin for FactionsPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<Faction>();
		app.register_type::<IsTemporaryFaction>();
		app.add_systems(Update,
		(
			create_solo_faction,
			update_temporary_factions,
		));
	}
}

#[derive(Component, Default, Clone, Copy, Debug, Reflect)]
pub struct Faction;

#[derive(Bundle)]
pub struct FactionBundle
{
	pub faction: Faction,
	pub name: Name,
}
impl FactionBundle
{
	pub fn new<N: Into<Name>>(name: N) -> Self
	{
		Self
		{
			name: name.into(),
			faction: default(),
		}
	}
}

#[derive(Component, Default, Reflect, Clone, Copy)]
pub struct IsTemporaryFaction;

pub fn create_solo_faction
(
	mut commands: Commands,
	orphaned_players: Query<(Entity, &Name), (With<Player>, Without<Parent>)>,
)
{
	for (player, name) in orphaned_players.iter()
	{
		commands.spawn
		((
			FactionBundle::new(format!("{} - Solo", name)),
			IsTemporaryFaction,
		)).add_child(player);
	}
}

pub fn update_temporary_factions
(
	mut commands: Commands,
	changed_names: Query
	<
		&Name,
		(
			Without<IsTemporaryFaction>,
			With<Player>,
			Changed<Name>,
		),
	>,
	mut factions: Query
	<
		(
			Entity,
			Option<&Children>,
			&mut Name,
		),
		(
			Without<Player>,
			With<IsTemporaryFaction>,
		),
	>,
)
{
	for (faction, members, mut name) in factions.iter_mut()
	{
		let member_count = members.map(|c| c.len()).unwrap_or(0);
		if member_count == 0 {commands.entity(faction).despawn();}
		else if let Ok(player_name) = changed_names
			.get(*members.unwrap().first().unwrap())
		{
			*name = Name::new(format!("{} - Solo", player_name));
		}
	}
}