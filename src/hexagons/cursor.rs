use bevy::prelude::*;
use bevy::render::camera::CameraProjection;
use bevy::window::*;
use super::*;

#[derive(Component, Default, Reflect)]
pub struct IsCursorOverHex;

#[derive(Component, Default, Reflect)]
pub struct FollowsCursor;

#[derive(Resource, Default, Reflect)]
pub struct CursorPosition
{
    pub position: Option<Vec2>,
    pub hex: Option<HexCoordinate>,
}

pub struct HexCursorPlugin;
impl Plugin for HexCursorPlugin
{
    fn build(&self, app: &mut App)
	{
        app.register_type::<IsCursorOverHex>();
        app.register_type::<FollowsCursor>();
        app.init_resource::<CursorPosition>();
        app.add_systems(PreUpdate, update_cursor_hex_system);
        app.add_systems(Update, cursor_over_hex_system);
    }
}

fn update_cursor_hex_system
(
    window: Query<&Window, With<PrimaryWindow>>,
    projection: Query<&OrthographicProjection>,
    hex_spacing: Res<HexSpacing>,
    mut cursor: ResMut<CursorPosition>,
)
{
    let cursor = cursor.as_mut();
    let projection = projection.single() as &dyn CameraProjection;
    let projection = projection.get_projection_matrix().inverse();
    let window = window.single();
    if let Some(mut window_cursor) = window.cursor_position()
	{
        window_cursor.x /= window.width();
        window_cursor.y /= window.height();
        window_cursor *= 2.0;
        window_cursor -= Vec2::splat(1.0);
        let projection_result = (projection * Vec4::new
		(
            window_cursor.x,
            -window_cursor.y,
            1.0,
            1.0
        )).xy();
        let hex = HexCoordinate::from_euclidean(projection_result / hex_spacing.0);
        cursor.hex = Some(hex.round());
        cursor.position = Some(projection_result);
    }
	else
	{
        cursor.hex = None;
        cursor.position = None;
    }
}

fn cursor_over_hex_system
(
    mut commands: Commands,
    query: Query
	<(
        Entity,
        &HexCoordinate,
        Option<&IsCursorOverHex>,
    )>,
    cursor_hex: Res<CursorPosition>,
)
{
    for (entity, entity_position, is_cursor_over_hex) in query.iter()
	{
        let should_have_cursor_flag = cursor_hex.hex.map_or(false, |cursor_position|
		{
            *entity_position == cursor_position
        });
        if is_cursor_over_hex.is_some() != should_have_cursor_flag
		{
            match should_have_cursor_flag
			{
                true =>
				{
                    commands.entity(entity).insert(IsCursorOverHex::default());
                },
                false =>
				{
                    commands.entity(entity).remove::<IsCursorOverHex>();
                },
            }
        }
    }
}