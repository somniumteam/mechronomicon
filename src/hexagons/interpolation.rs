use bevy::{prelude::*, transform::TransformSystem};
use super::*;

pub struct HexInterpolationPlugin;
impl Plugin for HexInterpolationPlugin
{
	fn build(&self, app: &mut App)
	{
        app.register_type::<HexInterpolationSpeed>();
		app.add_systems(PostUpdate,
			update_hex_position.before(TransformSystem::TransformPropagate)
		);
	}
}

#[derive(Component, Reflect)]
pub struct HexInterpolationSpeed(pub HexIntegral);
impl Default for HexInterpolationSpeed
{
	fn default() -> Self
	{
		Self(64.0)
	}
}

pub fn update_hex_position
(
	mut query: Query<(&mut Transform, &HexCoordinate, Option<&HexInterpolationSpeed>)>,
	spacing: Res<HexSpacing>,
	time: Res<Time>,
)
{
	for (mut transform, hex, speed) in query.iter_mut()
	{
		let target = hex.to_euclidean() * spacing.0;
		let current = transform.translation.xy();
		if target != current
		{
			if speed.is_none() ||
			{
				let delta = target - current;
				let dist_sqr = delta.length_squared();
				let speed = speed.unwrap().0;
				let tick_distance = speed * time.delta_seconds();
				if tick_distance * tick_distance >= dist_sqr
				{
					true
				}
				else
				{
					let tick_movement = (delta / dist_sqr.sqrt()) * tick_distance;
					transform.translation.x += tick_movement.x;
					transform.translation.y += tick_movement.y;
					false
				}
			}
			{
				transform.translation.x = target.x;
				transform.translation.y = target.y;
			}
		}
	}
}