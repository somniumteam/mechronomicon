use std::ops::*;
use std::f32::consts::*;
use bevy::prelude::*;
use lazy_static::lazy_static;

pub mod cursor;
pub use cursor::*;
pub mod interpolation;
pub use interpolation::*;

pub type HexIntegral = f32;

pub struct HexagonPlugin
{
	pub hex_spacing: HexIntegral,
}
impl Default for HexagonPlugin
{
	fn default() -> Self
	{
		HexagonPlugin
		{
			hex_spacing: 64.0,
		}
	}
}
impl Plugin for HexagonPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<HexCoordinate>();
        app.register_type::<HexSpacing>();
		app.insert_resource(HexSpacing(self.hex_spacing));
		app.add_plugins(HexCursorPlugin);
		app.add_plugins(HexInterpolationPlugin);
	}
}

#[derive(Resource, Reflect)]
pub struct HexSpacing(pub HexIntegral);

#[derive(Default, Debug, Copy, Clone, PartialEq, Reflect, Component)]
pub struct HexCoordinate
{
    pub q: HexIntegral,
    pub r: HexIntegral,
}

lazy_static!
{
    static ref EUCLIDEAN_COMPONENT_Q: Vec2 = Vec2 {x: f32::cos(FRAC_PI_6), y: -0.5};
    static ref EUCLIDEAN_COMPONENT_R: Vec2 = Vec2 {x: 0.0, y: -1.0};
    static ref INVERSE_EUCLIDEAN_COMPONENT_X: HexCoordinate = HexCoordinate {q: 1.0 / f32::cos(FRAC_PI_6), r: -0.5 / f32::cos(FRAC_PI_6)};
    static ref INVERSE_EUCLIDEAN_COMPONENT_Y: HexCoordinate = HexCoordinate {q: 0.0, r: -1.0};
}

impl HexCoordinate
{
    pub const NORTH_EAST: Self = Self {q: 1.0, r: -1.0};
    pub const NORTH: Self = Self {q: 0.0, r: -1.0};
    pub const NORTH_WEST: Self = Self {q: -1.0, r: 0.0};
    pub const SOUTH_WEST: Self = Self {q: -1.0, r: 1.0};
    pub const SOUTH: Self = Self {q: 0.0, r: 1.0};
    pub const SOUTH_EAST: Self = Self {q: 1.0, r: 0.0};
    pub const DIRECTIONS: [Self; 6] = [Self::NORTH_EAST, Self::NORTH, Self::NORTH_WEST, Self::SOUTH_WEST, Self::SOUTH, Self::SOUTH_EAST];

    pub fn s(self: &Self) -> HexIntegral
	{
        -self.q - self.r
    }

    pub fn manhattan_length(self: &Self) -> HexIntegral
	{
        (HexIntegral::abs(self.q) + HexIntegral::abs(self.r) + HexIntegral::abs(self.s())) / 2.0
    }

    pub fn manhattan_distance(self: &Self, rhs: Self) -> HexIntegral
	{
        (rhs - *self).manhattan_length()
    }

    pub fn reduce_to_adjacent(self: &Self) -> Self
	{
        let south_north = self.r - self.s();
        let south_north_abs = south_north.abs();
        let north_west_south_east = self.s() - self.q;
        let north_west_south_east_abs = north_west_south_east.abs();
        let north_east_south_west = self.q - self.r;
        let north_east_south_west_abs = north_east_south_west.abs();
        if self.q == 0.0 && self.r == 0.0 {Self::default()}
        else if south_north_abs >= north_west_south_east_abs.max(north_east_south_west_abs)
		{
            if south_north > 0.0 {Self::SOUTH}
            else {Self::NORTH}
        }
		else if north_west_south_east_abs >= north_east_south_west_abs
		{
            if north_west_south_east >= 0.0 {Self::NORTH_WEST}
            else {Self::SOUTH_EAST}
        }
		else if north_east_south_west >= 0.0 {Self::NORTH_EAST}
        else {Self::SOUTH_WEST}
    }

    pub fn round(self: &Self) -> Self
	{
        let self_s = self.s();
        let mut ret_val = Self
		{
            q: HexIntegral::round(self.q),
            r: HexIntegral::round(self.q),
        };
        let expected_s = HexIntegral::round(self_s);
        let q_diff = HexIntegral::abs(ret_val.q - self.q);
        let r_diff = HexIntegral::abs(ret_val.r - self.r);
        let s_diff = HexIntegral::abs(expected_s - self_s);
        if q_diff > r_diff && q_diff > s_diff
		{
            ret_val.q = -ret_val.r - expected_s;
        }
        else if r_diff > s_diff
		{
            ret_val.r = -ret_val.q - expected_s;
        }
        ret_val
    }

    pub fn lerp(self: &Self, rhs: &Self, ratio: HexIntegral) -> Self
	{
        *self + (*rhs - *self) * ratio
    }

    pub fn to_euclidean(self: &Self) -> Vec2
	{
        *EUCLIDEAN_COMPONENT_Q * self.q
        + *EUCLIDEAN_COMPONENT_R * self.r
    }

    pub fn from_euclidean(vec: Vec2) -> HexCoordinate
	{
        *INVERSE_EUCLIDEAN_COMPONENT_X * vec.x
        + *INVERSE_EUCLIDEAN_COMPONENT_Y * vec.y
    }

    pub fn spiral(max_dist: Option<u64>) -> impl Coroutine<Yield = HexCoordinate, Return = ()>
	{
        #[coroutine] move ||
		{
            yield HexCoordinate::default();
            let mut radius = 1;
            loop
			{
                let mut ret_val = Self::SOUTH * radius as HexIntegral;
                for direction in Self::DIRECTIONS.iter()
				{
                    for _ in 0..radius
					{
                        yield ret_val;
                        ret_val += *direction;
                    }
                }
                radius += 1;
                if let Some(max_dist) = max_dist
				{
                    if radius > max_dist {break;}
                }
            }
        }
    }
}

// Only a logic error when one or more properties are NaN, which results in undetermined behavior in all cases anyway.
impl Eq for HexCoordinate {}

// https://internals.rust-lang.org/t/f32-f64-should-implement-hash/5436/5
impl std::hash::Hash for HexCoordinate
{
	fn hash<H: std::hash::Hasher>(&self, state: &mut H)
	{
		unsafe
		{
			state.write_u32(std::mem::transmute(self.q));
			state.write_u32(std::mem::transmute(self.r));
		}
	}
}

impl Add for HexCoordinate
{
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output
	{
        Self
		{
            q: self.q + rhs.q,
            r: self.r + rhs.r,
        }
    }
}

impl AddAssign for HexCoordinate
{
    fn add_assign(&mut self, rhs: Self)
	{
        self.q += rhs.q;
        self.r += rhs.r;
    }
}

impl Sub for HexCoordinate
{
    type Output = Self;
    fn sub(self, rhs: Self) -> Self::Output
	{
        Self
		{
            q: self.q - rhs.q,
            r: self.r - rhs.r,
        }
    }
}

impl SubAssign for HexCoordinate
{
    fn sub_assign(&mut self, rhs: Self)
	{
        self.q -= rhs.q;
        self.r -= rhs.r;
    }
}

impl Mul<HexIntegral> for HexCoordinate
{
    type Output = Self;
    fn mul(self, rhs: HexIntegral) -> Self::Output
	{
        Self
		{
            q: self.q * rhs,
            r: self.r * rhs,
        }
    }
}

impl MulAssign<HexIntegral> for HexCoordinate
{
    fn mul_assign(&mut self, rhs: HexIntegral)
	{
        self.q *= rhs;
        self.r *= rhs;
    }
}

impl Div<HexIntegral> for HexCoordinate
{
    type Output = Self;
    fn div(self, rhs: HexIntegral) -> Self::Output
	{
        Self
		{
            q: self.q / rhs,
            r: self.r / rhs,
        }
    }
}

impl DivAssign<HexIntegral> for HexCoordinate
{
    fn div_assign(&mut self, rhs: HexIntegral)
	{
        self.q /= rhs;
        self.r /= rhs;
    }
}

impl Neg for HexCoordinate
{
    type Output = Self;
    fn neg(self) -> Self::Output
	{
        Self
		{
            q: -self.q,
            r: -self.r,
        }
    }
}