use bevy::prelude::*;
use crate::*;

pub struct QuickVersusPlugin;
impl Plugin for QuickVersusPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<QuickVersusLaunch>();
		app.add_event::<QuickVersusLaunch>();
		app.add_systems(Update, quick_versus_launch);
	}
}

#[derive(Event, Default, Reflect)]
pub struct QuickVersusLaunch;

pub fn quick_versus_launch
(
	mut launch_events: EventReader<QuickVersusLaunch>,
	mut connect_events: EventWriter<NetworkConnectEvent>,
)
{
	if !launch_events.is_empty()
	{
		launch_events.clear();
		connect_events.send(NetworkConnectEvent
		{
			extra_args: Some("next=2".into()),
			..default()
		});
	}
}