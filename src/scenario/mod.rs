use bevy::prelude::*;
use crate::*;

pub mod quick_play;
pub use quick_play::*;
pub mod quick_versus;
pub use quick_versus::*;

pub struct ScenarioPlugin;
impl Plugin for ScenarioPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<ScenarioScope>();
		app.register_type::<InScenario>();
		app.register_type::<ClearScenario>();
		app.add_event::<ClearScenario>();
		app.add_plugins(QuickPlayPlugin);
		app.add_plugins(QuickVersusPlugin);
		app.add_systems(Update,
		(
			clear_scenario,
		));
	}
}

#[derive(Resource, Default, Reflect)]
pub struct InScenario;

#[derive(Component, Default, Reflect)]
pub struct ScenarioScope;

#[derive(Event, Default, Reflect)]
pub struct ClearScenario;

pub fn clear_scenario
(
	mut commands: Commands,
	scenario_scoped: Query<Entity, With<ScenarioScope>>,
	mut events: EventReader<ClearScenario>,
	mut phase: ResMut<NextState<GamePhase>>,
	mut role_state: ResMut<NextState<NetworkRole>>,
)
{
	if events.read().count() != 0
	{
		for entity in scenario_scoped.iter()
		{
			commands.entity(entity).despawn_recursive();
		}
		commands.remove_resource::<InScenario>();
		phase.set(GamePhase::STAGING);
		role_state.set(NetworkRole::Authoritative);
	}
}