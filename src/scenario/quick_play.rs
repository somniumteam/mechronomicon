use bevy::prelude::*;
use bevy_egui::{egui, EguiContexts};
use lazy_static::lazy_static;
use include_bytes_plus::include_bytes;
use rand::Rng;
use crate::*;

lazy_static!
{
	pub static ref QUICK_PLAY_BRIEFING: String = String::from_utf8(include_bytes!("assets/quick_play_briefing.txt").to_vec()).unwrap_or(default());
}

pub struct QuickPlayPlugin;
impl Plugin for QuickPlayPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<QuickPlaySingletons>();
		app.register_type::<QuickPlayLaunch>();
		app.add_event::<QuickPlayLaunch>();
		app.add_systems(Update,
		(
			quick_play_clear.after(super::clear_scenario),
			quick_play_launch.after(quick_play_clear),
		));
		app.add_systems(Update,
			(
				quick_play_briefing.run_if(in_state(GamePhase::STAGING)),
			).run_if(resource_exists::<QuickPlaySingletons>)
		);
	}
}

#[derive(Resource, Clone, Reflect)]
pub struct QuickPlaySingletons
{
	pub player_image: Handle<Image>,
}

pub fn quick_play_clear
(
	mut commands: Commands,
	mut events: EventReader<super::ClearScenario>,
)
{
	if events.read().count() != 0
	{
		commands.remove_resource::<QuickPlaySingletons>();
	}
}

#[derive(Event, Default, Reflect)]
pub struct QuickPlayLaunch;

pub fn quick_play_launch
(
	mut commands: Commands,
	mut events: EventReader<QuickPlayLaunch>,
	asset_server: Res<AssetServer>,
	spacing: Res<HexSpacing>,
	local_player: Query<Entity, (With<Player>, With<IsLocalPlayer>)>,
)
{
	if events.read().count() != 0
	{
		commands.init_resource::<InScenario>();
		commands.insert_resource(TerrainState::new(100));
		let mut rng = rand::thread_rng();
		let singletons = QuickPlaySingletons
		{
			player_image: asset_server.load("Biological - Human 8x.png"),
		};
		let player_size = spacing.0 * 0.85;
		let local_player = local_player.single();
		let enemy_player = commands.spawn(NoopBotBundle::new("GMan")).id();
		let player_power_pools: PowerPools = vec!
		(
			(PowerPoolType::SPIRIT, vec!(5,6,7,8,9).into_iter().collect()),
			(PowerPoolType::WEAPONS, PowerPool::new_with_value(3, 1)),
			(PowerPoolType::DEFENSES, PowerPool::new_with_value(3, 2)),
			(PowerPoolType::LOCOMOTION, PowerPool::new_with_value(3, 3)),
			(PowerPoolType::COMMUNICATION, PowerPool::new_with_value(3, 4)),
		).into_iter().collect();
		commands.spawn
		((
			MechBundle
				::new
				(
					SpriteBundle::new
					(
						singletons.player_image.clone(),
						Vec2::splat(player_size),
					),
					local_player,
				)
				.with_power_pools(player_power_pools),
			ScenarioScope,
		));
		let rand_pos = HexCoordinate
		{
			q: rng.gen_range(-5..5) as HexIntegral,
			r: rng.gen_range(-5..5) as HexIntegral,
		};
		commands.spawn
		((
			MechBundle
				::new
				(
					SpriteBundle::new
					(
						singletons.player_image.clone(),
						Vec2::splat(player_size),
					),
					enemy_player,
				)
				.with_hex(rand_pos)
				.with_power_pool_same_value(3),
			ScenarioScope,
		));
		for hex in std::iter::from_coroutine(HexCoordinate::spiral(Some(10)))
		{
			let r = rng.gen_range(0..12);
			let tile_type = if r <= 8 {"grass"}
			else {"blank"};
			if tile_type != "blank"
			{
				let tile_state = TerrainTileState
				{
					base_type: tile_type.into(),
					..default()
				};
				commands.modify_terrain_ext(hex, tile_state, HexInterpolationSpeed(256.0));
			}
		}
		commands.insert_resource(singletons);
	}
}

pub fn quick_play_briefing
(
	mut egui: EguiContexts,
	mut phase: ResMut<NextState<GamePhase>>,

)
{
	let ctx = egui.ctx_mut();
	let default_pos = ctx.available_rect().right_top();
	egui::Window::new("Briefing")
		.id("QuickPlayBriefing".into())
		.collapsible(false)
		.max_width(600.0)
		.default_pos(default_pos)
		.resizable([false, true])
		.scroll2([false, true])
		.pivot(egui::Align2::RIGHT_TOP)
		.show(ctx,|ui| ui.with_layout
		(
			egui::Layout
			{
				main_dir: egui::Direction::TopDown,
				..default()
			},
			|ui|
			{
				ui.label(QUICK_PLAY_BRIEFING.to_owned());
				ui.separator();
				if ui.button("Begin!").clicked()
				{
					phase.set(GamePhase::INITIATIVE);
				}
			},
		));
}