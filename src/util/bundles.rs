use bevy::prelude::*;

pub trait SpriteBundleExt
{
	fn new(image: Handle<Image>, size: Vec2) -> Self;
	fn with_z_offset(self, z_offset: f32) -> Self;
}
impl SpriteBundleExt for SpriteBundle
{
	fn new(image: Handle<Image>, size: Vec2) -> Self
	{
		Self
		{
			texture: image,
			sprite: Sprite
			{
				custom_size: Some(size),
				..default()
			},
			..default()
		}
	}

	fn with_z_offset(mut self, z_offset: f32) -> Self
	{
		self.transform.translation.z = z_offset;
		self
	}
}