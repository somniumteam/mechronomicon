use rand::Rng;

pub mod bundles;
pub use bundles::*;

pub type UUID = u64;

pub fn new_uuid() -> UUID
{
	let mut rng = rand::thread_rng();
	rng.gen()
}