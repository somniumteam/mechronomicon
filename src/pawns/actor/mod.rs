use std::borrow::Cow;
use bevy::prelude::*;
use crate::*;

pub const ACTOR_INDICATOR_HEIGHT: f32 = 2.0;
pub const ACTOR_COMMAND_MARKER_SIZE: Vec2 = Vec2::splat(32.0);
pub const CAN_SELECT_INDICATOR_SIZE: Vec2 = Vec2::splat(16.0);
pub const CAN_SELECT_INDICATOR_OFFSET: Vec2 = Vec2::new(10.0, 20.0);
pub const SELECTION_INDICATOR_HEIGHT: f32 = DEFAULT_PAWN_LAYER_OFFSET - 0.25;

pub struct ActorPlugin;
impl Plugin for ActorPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<ActorSingletons>();
		app.register_type::<Actor>();
		app.register_type::<IsCanSelectIndicator>();
		app.register_type::<IsSelected>();
		app.register_type::<IsSelectionIndicator>();
		app.add_systems(Startup, setup);
		app.add_systems(Update,
		(
			(
				actor_selection,
				selection_indicator,
			).run_if(not(resource_exists::<IsEguiCapturingMouse>)),
			selection_clear_on_menu_open,
		));
	}
}
#[derive(Resource, Reflect)]
pub struct ActorSingletons
{
	pub can_select_indicator_image: Handle<Image>,
	pub selection_indicator_image: Handle<Image>,
}
fn setup
(
	mut commands: Commands,
	asset_server: Res<AssetServer>,
)
{
	commands.insert_resource(ActorSingletons
	{
		can_select_indicator_image: asset_server.load("find-icon.png"),
		selection_indicator_image: asset_server.load("object-selected-icon.png"),
	});
}

#[derive(Bundle)]
pub struct ActorBundle
{
	pub pawn: PawnBundle,
	pub actor: Actor,
}
impl ActorBundle
{
	pub fn new
	(
		sprite: SpriteBundle,
		controller: Entity,
	) -> Self
	{
		Self
		{
			pawn: PawnBundle::new(sprite)
				.with_name("Unnamed Actor"),
			actor: Actor
			{
				controller,
				command_queue: default(),
			},
		}
	}

	pub fn with_hex(mut self, hex: HexCoordinate) -> Self
	{
		self.pawn = self.pawn.with_hex(hex);
		self
	}

	pub fn with_name(mut self, name: impl Into<Cow<'static, str>>) -> Self
	{
		self.pawn = self.pawn.with_name(name);
		self
	}
}
#[derive(Component, Reflect)]
pub struct Actor
{
	pub controller: Entity,
	pub command_queue: Vec<ActorCommand>,
}

#[derive(Component, Default, Reflect)]
pub struct IsCanSelectIndicator;
#[derive(Bundle)]
pub struct CanSelectIndicatorBundle
{
	pub sprite: SpriteBundle,
	pub is_can_select_indicator: IsCanSelectIndicator,
}
impl CanSelectIndicatorBundle
{
	pub fn new(singletons: &ActorSingletons) -> Self
	{
		Self
		{
			sprite: SpriteBundle
			{
				sprite: Sprite
				{
					custom_size: Some(CAN_SELECT_INDICATOR_SIZE),
					..default()
				},
				texture: singletons.can_select_indicator_image.clone(),
				transform: Transform::from_translation(CAN_SELECT_INDICATOR_OFFSET.extend(ACTOR_INDICATOR_HEIGHT)),
				..default()
			},
			is_can_select_indicator: default(),
		}
	}
}

#[derive(Component, Default, Reflect)]
pub struct IsSelected;

pub fn actor_selection
(
	mut commands: Commands,
	singletons: Res<ActorSingletons>,
	existing_indicator: Query<(Entity, &Parent), With<IsCanSelectIndicator>>,
	hovering: Query<(Entity, &Actor), With<IsCursorOverHex>>,
	local_player: Query<Entity, With<IsLocalPlayer>>,
	selected: Query<Entity, With<IsSelected>>,
    clicks: Res<ButtonInput<MouseButton>>,
)
{
	let tried_selection = clicks.just_pressed(MouseButton::Left);
	if tried_selection && !selected.is_empty() {commands.entity(selected.single()).remove::<IsSelected>();}
	let local_player = local_player.single();
	let hovering = hovering.iter().find_map(|(entity, actor)|
	{
		if actor.controller == local_player {Some(entity)}
		else {None}
	});
	let existing_indicator = if existing_indicator.is_empty() {None}
	else {Some(existing_indicator.single())};
	if let Some(hovering) = hovering
	{
		if existing_indicator.is_none()
		{
			commands.entity(hovering).with_children(|builder|
			{
				builder.spawn(CanSelectIndicatorBundle::new(&singletons));
			});
		}
		else
		{
			let existing_indicator = existing_indicator.unwrap();
			if existing_indicator.1.get() != hovering {commands.entity(existing_indicator.0).despawn_recursive();}
		}
		if tried_selection {commands.entity(hovering).insert(IsSelected);}
	}
	else if existing_indicator.is_some() {commands.entity(existing_indicator.unwrap().0).despawn_recursive();}
}

#[derive(Component, Default, Reflect)]
pub struct IsSelectionIndicator;
#[derive(Bundle)]
pub struct SelectionIndicatorBundle
{
	pub sprite: SpriteBundle,
	pub is_selection_indicator: IsSelectionIndicator,
}
impl SelectionIndicatorBundle
{
	pub fn new(singletons: &ActorSingletons, hex_spacing: HexIntegral) -> Self
	{
		Self
		{
			sprite: SpriteBundle
			{
				texture: singletons.selection_indicator_image.clone(),
				sprite: Sprite
				{
					custom_size: Some(Vec2::splat(hex_spacing)),
					..default()
				},
				transform: Transform::from_translation(Vec2::default().extend(SELECTION_INDICATOR_HEIGHT)),
				..default()
			},
			is_selection_indicator: default(),
		}
	}
}

pub fn selection_indicator
(
	mut commands: Commands,
	singletons: Res<ActorSingletons>,
	selected: Query<(Entity, &Children), With<IsSelected>>,
	indicators: Query<(Entity, &Parent), With<IsSelectionIndicator>>,
	hex_spacing: Res<HexSpacing>,
)
{
	for (indicator, parent) in indicators.iter()
	{
		if !selected.contains(parent.get())
		{
			commands.entity(indicator).despawn_recursive();
		}
	}
	for (selected, children) in selected.iter()
	{
		let mut found_indicator = false;
		for child in children.iter()
		{
			if indicators.contains(*child)
			{
				found_indicator = true;
				break;
			}
		}
		if !found_indicator
		{
			commands.entity(selected).with_children(|builder|
			{
				builder.spawn(SelectionIndicatorBundle::new(&singletons, hex_spacing.0));
			});
		}
	}
}

pub fn selection_clear_on_menu_open
(
	mut commands: Commands,
	mut events: EventReader<ToggleMainMenuEvent>,
	selected: Query<Entity, With<IsSelected>>,
)
{
	if !events.is_empty()
	{
		if let Some(selected) = selected.get_single().ok()
		{
			commands.entity(selected).remove::<IsSelected>();
		}
		
	}
	events.clear();
}