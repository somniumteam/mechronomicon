use std::borrow::Cow;
use bevy::prelude::*;
use crate::*;
use lazy_static::lazy_static;

lazy_static!
{
	pub static ref DEFAULT_MECH_POWER_POOL_S: PowerPools = PowerPools(vec!
	(
		(PowerPoolType::WEAPONS, PowerPool::new(3)),
		(PowerPoolType::DEFENSES, PowerPool::new(3)),
		(PowerPoolType::LOCOMOTION, PowerPool::new(3)),
		(PowerPoolType::ACTUATORS, PowerPool::new(3)),
		(PowerPoolType::COMMUNICATION, PowerPool::new(3)),
	));
}

pub const MECH_INTERPOLATION_SPEED_MIN: HexIntegral = 64.;
pub const MECH_INTERPOLATION_SPEED_MAX: HexIntegral = 1024.;
pub const MECH_INTERPOLATION_SPEED_FACTOR: HexIntegral = (MECH_INTERPOLATION_SPEED_MAX - MECH_INTERPOLATION_SPEED_MIN) / 18.;

#[derive(Component, Default, Reflect, Copy, Clone)]
pub struct IsMech;

#[derive(Bundle)]
pub struct MechBundle
{
	pub is_mech: IsMech,
	pub actor: ActorBundle,
	pub power_pools: PowerPools,
}
impl MechBundle
{
	pub fn new(sprite: SpriteBundle, controller: Entity) -> Self
	{
		Self
		{
			is_mech: default(),
			actor: ActorBundle::new(sprite, controller)
				.with_name("Unnamed Mech"),
			power_pools: DEFAULT_MECH_POWER_POOL_S.clone(),
		}
	}

	pub fn with_hex(mut self, hex: HexCoordinate) -> Self
	{
		self.actor = self.actor.with_hex(hex);
		self
	}

	pub fn with_name(mut self, name: impl Into<Cow<'static, str>>) -> Self
	{
		self.actor = self.actor.with_name(name);
		self
	}

	pub fn with_power_pools(mut self, power_pools: PowerPools) -> Self
	{
		self.power_pools = power_pools;
		self
	}

	pub fn with_power_pool_same_value(mut self, value: AllocationIntegral) -> Self
	{
		let power_pools = &mut self.power_pools;
		for (_, power_pool) in power_pools.0.iter_mut()
		{
			for allocation in power_pool.0.iter_mut()
			{
				allocation.value = value;
			}
		}
		self
	}
}

pub struct MechPlugin;
impl Plugin for MechPlugin
{
	fn build(&self, app: &mut App)
	{
		app.add_systems(OnEnter(GamePhase::PLANNING), update_mech_interpolation_speed);
	}
}

pub fn update_mech_interpolation_speed
(
	mut commands: Commands,
	mut mechs: Query
	<
		(
			Entity,
			&PowerPools,
			Option<&mut HexInterpolationSpeed>,
		),
		(
			Or<(Changed<PowerPools>, Added<PowerPools>)>,
			With<IsMech>,
		),
	>,
)
{
	for (entity, pools, interpolation) in mechs.iter_mut()
	{
		let locomotion: HexIntegral = pools.get(PowerPoolType::LOCOMOTION)
			.map(|pool| pool.total())
			.unwrap_or(0)
			.into();
		let desired_speed = MECH_INTERPOLATION_SPEED_FACTOR * locomotion + MECH_INTERPOLATION_SPEED_MIN;
		let current_speed = interpolation.as_ref().map(|h| h.0).unwrap_or(0.);
		if current_speed != desired_speed
		{
			if let Some(mut interpolation) = interpolation
			{
				interpolation.0 = desired_speed;
			}
			else
			{
				commands.entity(entity).insert(HexInterpolationSpeed(desired_speed));
			}
		}
	}
}