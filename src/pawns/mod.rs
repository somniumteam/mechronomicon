use std::borrow::Cow;
use bevy::prelude::*;
use crate::*;

pub mod actor;
pub use actor::*;
pub mod power_pools;
pub use power_pools::*;
pub mod mech;
pub use mech::*;

pub const DEFAULT_PAWN_LAYER_OFFSET: f32 = 1.0;

pub struct PawnPlugin;
impl Plugin for PawnPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<IsPawn>();
		app.add_plugins(ActorPlugin);
		app.add_plugins(PowerPoolPlugin);
		app.add_plugins(MechPlugin);
	}
}

#[derive(Component, Default, Reflect)]
pub struct IsPawn;

#[derive(Bundle)]
pub struct PawnBundle
{
	pub sprite: SpriteBundle,
	pub hex: HexCoordinate,
	pub is_pawn: IsPawn,
	pub name: Name,
}
impl PawnBundle
{
	pub fn new(sprite: SpriteBundle) -> Self
	{
		Self
		{
			sprite: sprite.with_z_offset(DEFAULT_PAWN_LAYER_OFFSET),
			hex: default(),
			is_pawn: default(),
			name: Name::new("Pawn"),
		}
	}

	pub fn with_hex(mut self, hex: HexCoordinate) -> Self
	{
		self.hex = hex;
		self
	}

	pub fn with_name(mut self, name: impl Into<Cow<'static, str>>) -> Self
	{
		self.name.set(name);
		self
	}
}