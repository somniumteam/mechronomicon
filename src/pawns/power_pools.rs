use bevy::prelude::*;

pub type AllocationIntegral = u8;
pub const ALLOCATION_MAXIMUM: AllocationIntegral = 6;

pub struct PowerPoolPlugin;
impl Plugin for PowerPoolPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<Allocation>();
		app.register_type::<PowerPool>();
		app.register_type::<PowerPoolType>();
		app.register_type::<PowerPools>();
	}
}

#[derive(Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Reflect, Debug)]
pub struct Allocation
{
	pub value: AllocationIntegral,
	pub is_pledged: bool,
}
impl Allocation
{
	pub fn new(value: AllocationIntegral) -> Self
	{
		Self
		{
			value,
			is_pledged: default(),
		}
	}
}
impl Into<AllocationIntegral> for Allocation
{
	fn into(self) -> AllocationIntegral {self.value}
}
impl From<AllocationIntegral> for Allocation
{
	fn from(value: AllocationIntegral) -> Self {Self::new(value)}
}

#[derive(Default, Clone, Reflect, Debug)]
pub struct PowerPool(pub Vec<Allocation>);
impl PowerPool
{
	pub fn new(allocations: usize) -> Self
	{
		Self(vec![default(); allocations])
	}

	pub fn new_with_value(allocations: usize, value: AllocationIntegral) -> Self
	{
		Self(vec![Allocation::from(value); allocations])
	}

	pub fn iter<'a>(&'a self) -> std::slice::Iter<'a, Allocation>
	{
		self.0.iter()
	}

	pub fn iter_mut<'a>(&'a mut self) -> std::slice::IterMut<'a, Allocation>
	{
		self.0.iter_mut()
	}

	pub fn get_pledged<'a>(&'a mut self) -> impl Iterator<Item = &mut Allocation> + 'a
	{
		self.iter_mut().filter_map(|s| if s.is_pledged {Some(s)} else {None})
	}

	pub fn clear_pledges(&mut self)
	{
		self.iter_mut().for_each(|s| s.is_pledged = false);
	}

	pub fn total(&self) -> AllocationIntegral
	{
		self.iter().map(|a| a.value).sum()
	}
}
impl FromIterator<AllocationIntegral> for PowerPool
{
	fn from_iter<T: IntoIterator<Item = AllocationIntegral>>(iter: T) -> Self
	{
		Self(Vec::from_iter(iter.into_iter().map(|s| Allocation::from(s))))
	}
}

#[derive(Clone, Copy, PartialEq, Eq, Reflect, Debug)]
pub enum PowerPoolType
{
	SPIRIT,
	WEAPONS,
	DEFENSES,
	LOCOMOTION,
	ACTUATORS,
	COMMUNICATION,
}

pub const POWER_POOL_COLORS: [Color; 6] =
[
	Color::WHITE,
	Color::RED,
	Color::BLUE,
	Color::RgbaLinear {red: 0.35, green: 0.0, blue: 0.65, alpha: 1.0},
	Color::GREEN,
	Color::YELLOW,
];
pub fn get_power_pool_color(power_pool_type: PowerPoolType) -> Color
{
	POWER_POOL_COLORS[power_pool_type as usize]
}

pub const POWER_POOL_NAMES: [&str; 6] =
[
	"SPIRIT",
	"WEAPONS",
	"DEFENSES",
	"LOCOMOTION",
	"ACTUATORS",
	"COMMUNICATIONS",
];
pub fn get_power_pool_name(power_pool_type: PowerPoolType) -> &'static str
{
	POWER_POOL_NAMES[power_pool_type as usize]
}

pub type LabeledPowerPool = (PowerPoolType, PowerPool);

#[derive(Component, Clone, Reflect, Debug)]
pub struct PowerPools(pub Vec<LabeledPowerPool>);
impl PowerPools
{
	pub fn get(&self, power_pool_type: PowerPoolType) -> Option<&PowerPool>
	{
		self.0.iter().find_map(|pair|
			if pair.0 == power_pool_type {Some(&pair.1)}
			else {None}
		)
	}

	pub fn get_mut(&mut self, power_pool_type: PowerPoolType) -> Option<&mut PowerPool>
	{
		self.0.iter_mut().find_map(|pair|
			if pair.0 == power_pool_type {Some(&mut pair.1)}
			else {None}
		)
	}

	pub fn iter<'a>(&'a self) -> std::slice::Iter<'a, LabeledPowerPool>
	{
		self.0.iter()
	}

	pub fn iter_mut<'a>(&'a mut self) -> std::slice::IterMut<'a, LabeledPowerPool>
	{
		self.0.iter_mut()
	}

	pub fn get_pledged<'a>(&'a mut self) -> impl Iterator<Item = &mut Allocation> + 'a
	{
		self.iter_mut().map(|a| a.1.get_pledged()).flatten()
	}

	pub fn clear_pledges(&mut self)
	{
		self.iter_mut().for_each(|a| a.1.clear_pledges());
	}
}
impl FromIterator<LabeledPowerPool> for PowerPools
{
	fn from_iter<T: IntoIterator<Item = LabeledPowerPool>>(iter: T) -> Self
	{
		Self(Vec::from_iter(iter.into_iter()))
	}
}