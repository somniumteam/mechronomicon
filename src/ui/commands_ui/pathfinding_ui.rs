use bevy::prelude::*;
use bevy_svg::prelude::*;
use crate::*;

pub const PATHFINDING_MARKER_DEFAULT_TRANSFORM: Transform = Transform::from_translation(Vec2::ZERO.extend(20.));
pub const PATHFINDING_MARKER_DESTINATION_SCALE: f32 = 0.65;
pub const PATHFINDING_MARKER_PATH_SCALE: f32 = 0.25;

pub struct PathfindingUIPlugin;
impl Plugin for PathfindingUIPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<PathfindingUISingletons>();
		app.register_type::<PathfindingRender>();
		app.add_systems(Startup, setup);
		app.add_systems(Update, render_move_commands);
	}
}

#[derive(Resource, Reflect)]
pub struct PathfindingUISingletons
{
	pub move_marker: Handle<Svg>,
	pub move_marker_raw_size: Vec2,
	pub path_marker: Handle<Svg>,
	pub path_marker_raw_size: Vec2,
}

fn setup
(
	mut commands: Commands,
	asset_server: Res<AssetServer>,
)
{
	commands.insert_resource(PathfindingUISingletons
	{
		move_marker: asset_server.load("map-pin-direction-bottom-icon.svg"),
		move_marker_raw_size: Vec2::new(92.0, 122.0),
		path_marker: asset_server.load("map-direction-navigation-bottom-icon.svg"),
		path_marker_raw_size: Vec2::new(507.0, 512.0),
	});
}

#[derive(Component, Reflect)]
pub struct PathfindingRender
{
	pub requestor: Entity,
	pub request_template: PathfindingRequest,
}

pub fn render_move_commands
(
	mut commands: Commands,
	singletons: Res<PathfindingUISingletons>,
	requests: Query<(Entity, &PathfindingRequest)>,
	renders: Query<(Entity, &PathfindingRender, Option<&Children>)>,
	hex_spacing: Res<HexSpacing>,
)
{
	let mut requests_needing_render = requests.iter()
		.collect::<std::collections::HashMap<_, _>>();
	for (render_entity, render, children) in renders.iter()
	{
		let &PathfindingRender
		{
			requestor,
			ref request_template,
			..
		} = render;
		requests_needing_render.remove(&requestor);
		if let Some((_, request)) = requests.get(requestor).ok()
		{
			if !request.matches_template(request_template)
			{
				commands.entity(render_entity).despawn_recursive();
				continue;
			}
			if let Some(Ok(path)) = request.result()
			{
				if children.is_none()
				{
					path.into_iter().rev().enumerate().for_each(|(i, hex)|
					{
						let is_dest = i == 0;
						let raw_size = if is_dest {singletons.move_marker_raw_size} else {singletons.path_marker_raw_size};
						let scale = if is_dest {PATHFINDING_MARKER_DESTINATION_SCALE} else {PATHFINDING_MARKER_PATH_SCALE};
						let scale = scale * hex_spacing.0 / raw_size.y;
						let offset = Origin::Center.compute_translation(raw_size * scale);
						let svg = if is_dest {singletons.move_marker.clone()} else {singletons.path_marker.clone()};
						commands.entity(render_entity).with_children(|builder|
						{
							builder
								.spawn
								((
									TransformBundle::IDENTITY,
									InheritedVisibility::default(),
									hex,
								))
								.with_children(|builder|
								{
									builder.spawn
									((
										Svg2dBundle
										{
											svg,
											transform: Transform
												::from_scale(Vec3::splat(scale))
												.with_translation(offset),
											..default()
										},
									));
								});
						});
					});
				}
			}
		}
		else
		{
			commands.entity(render_entity).despawn_recursive();
			continue;
		}
	}
	for (entity, request) in requests_needing_render
	{
		commands.spawn
		((
			Name::new("Pathfinding Display"),
			PathfindingRender
			{
				requestor: entity,
				request_template: PathfindingRequest::from_template(request),
			},
			InheritedVisibility::VISIBLE,
			TransformBundle::from_transform(PATHFINDING_MARKER_DEFAULT_TRANSFORM),
		));
	}
}