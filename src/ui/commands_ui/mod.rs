use bevy::prelude::*;

pub mod pathfinding_ui;
pub use pathfinding_ui::*;

pub struct OrdersUIPlugin;
impl Plugin for OrdersUIPlugin
{
	fn build(&self, app: &mut App)
	{
		app.add_plugins(PathfindingUIPlugin);
	}
}