use bevy_egui::egui::{InnerResponse, Ui};
use crate::*;

pub trait WithDicierFontExtension
{
	fn set_dicier_font(&mut self) -> &mut Self;
	fn with_dicier_font<R>(&mut self, add_contents: impl FnOnce(&mut Self) -> R) -> InnerResponse<R>;
}

impl WithDicierFontExtension for Ui
{
	fn set_dicier_font(&mut self) -> &mut Self
	{
		self.style_mut().override_text_style = Some(bevy_egui::egui::TextStyle::Name(DICIER_FONT_NAME.into()));
		self
	}
	fn with_dicier_font<R>(&mut self, add_contents: impl FnOnce(&mut Self) -> R) -> InnerResponse<R>
	{
		self.scope(|ui|
		{
			ui.set_dicier_font();
			add_contents(ui)
		})
	}
}