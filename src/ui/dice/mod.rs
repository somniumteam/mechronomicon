use bevy::app::Plugin;
use bevy_egui::egui::*;
use crate::{AutoSizedWithLayoutExtension, DieIntegral};

pub mod with_dicier_font;
pub use with_dicier_font::*;
pub mod power_pool_display;
pub use power_pool_display::*;
pub mod check_display;
pub use check_display::*;
pub mod history;
pub use history::*;

pub struct DiceUIPlugin;
impl Plugin for DiceUIPlugin
{
	fn build(&self, app: &mut bevy::prelude::App)
	{
		app.add_plugins(DiceHistoryPlugin);
	}
}

#[derive(Clone, Copy)]
struct DiceWidgetEntry
{
	die: DieIntegral,
	color: Color32,
}
#[derive(Default)]
pub struct DiceWidget<'a>
{
	dice: Vec<DiceWidgetEntry>,
	clicked_iter_out: Option<&'a mut [bool]>,
}
impl<'a> DiceWidget<'a>
{
	pub fn new<D: IntoIterator<Item = DieIntegral>>(dice: D) -> Self
	{
		Self::new_with_colors(dice.into_iter().map(|d| (d, Color32::WHITE)))
	}
	pub fn new_with_colors<D: IntoIterator<Item = (DieIntegral, Color32)>>(dice: D) -> Self
	{
		Self
		{
			dice: dice.into_iter().map(|(die, color)| DiceWidgetEntry {die, color}).collect(),
			clicked_iter_out: None,
		}
	}

	pub fn with_clicked_iter_out(mut self, clicked_iter: &'a mut [bool]) -> Self
	{
		self.clicked_iter_out = Some(clicked_iter);
		self
	}

	pub fn dice_iter(&self) -> impl Iterator<Item = &DieIntegral>
	{
		self.dice.iter().map(|inner| &inner.die)
	}
	pub fn dice_iter_mut(&mut self) -> impl Iterator<Item = &mut DieIntegral>
	{
		self.dice.iter_mut().map(|inner| &mut inner.die)
	}

	pub fn color_iter(&self) -> impl Iterator<Item = &Color32>
	{
		self.dice.iter().map(|inner| &inner.color)
	}
	pub fn color_iter_mut(&mut self) -> impl Iterator<Item = &mut Color32>
	{
		self.dice.iter_mut().map(|inner| &mut inner.color)
	}
}
impl<'a> Widget for DiceWidget<'a>
{
	fn ui(self, ui: &mut Ui) -> Response
	{
		ui.auto_sized_with_layout
		(
			Layout::left_to_right(Align::Center),
			|ui|
			{
				ui.set_dicier_font();
				ui.spacing_mut().item_spacing = Vec2::ZERO;
				let ui_iter = self.dice_iter().zip(self.color_iter())
					.map(|(die, color)|
					{
						ui.colored_label(*color, format!("{}", die)).clicked()
					})
					.collect::<Vec<_>>()
					.into_iter();
				if let Some(clicked_iter_out) = self.clicked_iter_out
				{
					ui_iter.zip(clicked_iter_out).for_each(|(actual, reported)| *reported = actual);
				}
				else
				{
					ui_iter.count();
				}
			}
		).response
	}
}