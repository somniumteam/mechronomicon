use bevy::prelude::*;
use bevy::ecs::system::Command;
use bevy_egui::{egui, EguiContexts};
use bevy_egui::egui::Widget;
use crate::*;

pub const CHECK_HISTORY_MAXIMUM: usize = 30;

pub struct DiceHistoryPlugin;
impl Plugin for DiceHistoryPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<CheckHistoryItem>();
		app.register_type::<CheckHistory>();
		app.init_resource::<CheckHistory>();
		app.add_systems(Update, render_check_history);
	}
}

#[derive(Reflect, Clone)]
pub struct CheckHistoryItem
{
	pub name: String,
	pub check: Check,
}

#[derive(Resource, Reflect, Default)]
pub struct CheckHistory
{
	pub history: Vec<CheckHistoryItem>,
}

pub struct CheckHistoryWidget<'a>
{
	pub history: &'a CheckHistory,
}
impl<'a> CheckHistoryWidget<'a>
{
	pub fn new(history: &'a CheckHistory) -> Self
	{
		Self
		{
			history,
		}
	}
}
impl<'a> egui::Widget for CheckHistoryWidget<'a>
{
	fn ui(self, ui: &mut egui::Ui) -> egui::Response
	{
		ui.scope(|ui|
		{
			for item in self.history.history.iter()
			{
				ui.check_display(item.name.to_owned(), &item.check);
			}
		}).response
	}
}

pub fn render_check_history
(
	mut history: ResMut<CheckHistory>,
	mut egui: EguiContexts,
)
{
	while history.history.len() > CHECK_HISTORY_MAXIMUM {history.history.remove(0);}
	let ctx = egui.ctx_mut();
	let default_pos = ctx.available_rect().right_bottom();
	egui::Window::new("Check History")
		.pivot(egui::Align2::RIGHT_BOTTOM)
		.default_pos(default_pos)
		.collapsible(true)
		.resizable(true)
		.min_height(300.0)
		.default_width(0.0)
		.show(ctx, |ui|
		{
			egui::ScrollArea::new(egui::Vec2b::new(false, true))
				.auto_shrink(egui::Vec2b::FALSE)
				.stick_to_bottom(true)
				.show(ui, |ui|
				{
					CheckHistoryWidget::new(&history).ui(ui);
				});
		});
}

pub trait RetireCheckExtension
{
	fn retire_check(&mut self, check: Entity) -> &mut Self;
}
impl<'w, 's> RetireCheckExtension for Commands<'w, 's>
{
	fn retire_check(&mut self, check: Entity) -> &mut Self
	{
		self.add(RetireCheckCommand
		{
			check,
		});
		self
	}
}
pub struct RetireCheckCommand
{
	check: Entity,
}
impl Command for RetireCheckCommand
{
	fn apply(self, world: &mut World)
	{
		let entity = world.entity(self.check);
		let name = entity.get::<Name>().unwrap().to_owned();
		let check = entity.get::<Check>().unwrap().to_owned();
		let mut history = world.resource_mut::<CheckHistory>();
		history.history.push(CheckHistoryItem
		{
			check,
			name: name.into(),
		});
		despawn_with_children_recursive(world, self.check);
	}
}