use bevy::prelude::Color;
use bevy_egui::egui::*;
use crate::{AutoSizedWithLayoutExtension, BevyColorToEguiColorExtension};
use crate::{Check, CheckPhase};
use crate::{DiceWidget, DICIER_ANNOTATION_FONT_NAME};
use crate::{DieIntegral, AllocationIntegral};
use crate::PLEDGED_COLOR;

pub const CHECK_RESULT_SUCCESS_COLOR: Color = Color::GREEN;
pub const CHECK_RESULT_FAILURE_COLOR: Color = Color::RED;
pub const CHECK_RESULT_DEFAULT_COLOR: Color = Color::rgb(0.8, 0.8, 0.8);
pub const CHECK_RESULT_DISABLED_COLOR: Color = Color::GRAY;
pub const CHECK_RESULT_BUTTON_TEXT_COLOR: Color = Color::BLACK;
pub const CHECK_RESULT_BOOST_COLOR: Color = Color::GRAY;
pub const CHECK_RESULT_SUBMIT_COLOR: Color = Color::GREEN;
pub const CHECK_INSTRUCTIONS_COLOR: Color = Color::rgb(0.5, 0.8, 1.0);

pub trait CheckDisplayExtension
{
	fn check_display<S: Into<String>>(&mut self, label: S, check: &Check) -> Response;
}
impl CheckDisplayExtension for Ui
{
	fn check_display<S: Into<String>>(&mut self, label: S, check: &Check) -> Response
	{
		CheckDisplayWidget::new(label, check).ui(self)
	}
}

pub struct CheckDisplayWidget<'a>
{
	label: String,
	check: &'a Check,
	pledges: Option<(&'a[AllocationIntegral], &'a mut[bool])>,
	boosting_state: Option<&'a mut CheckPhase>,
}
impl<'a> CheckDisplayWidget<'a>
{
	pub fn new<S: Into<String>>(label: S, check: &'a Check) -> Self
	{
		Self
		{
			label: label.into(),
			check,
			pledges: Default::default(),
			boosting_state: Default::default(),
		}
	}

	pub fn with_pledges(mut self, pledge_values: &'a[AllocationIntegral], pledge_cancellations: &'a mut[bool]) -> Self
	{
		self.pledges = Some((pledge_values, pledge_cancellations));
		self
	}

	pub fn with_boost_and_submit_button(mut self, boosting_state: &'a mut CheckPhase) -> Self
	{
		self.boosting_state = Some(boosting_state);
		self
	}
}
impl<'a> Widget for CheckDisplayWidget<'a>
{
	fn ui(self, ui: &mut Ui) -> Response
	{
		let Self
		{
			label,
			check,
			pledges,
			boosting_state,
		} = self;
		let mut check = check.clone();
		if check.dice.is_none() {check.roll();}
		let dice = check.dice.as_ref().unwrap();
		let mut pledge_total = 0;
		if let Some((pledge_values, _)) = pledges
		{
			pledge_values.iter().for_each(|v| pledge_total += *v);
		}
		let result_color = match check.success_threshhold.is_some()
		{
			false => CHECK_RESULT_DEFAULT_COLOR,
			true => match check.predict_success(pledge_total)
			{
				true => CHECK_RESULT_SUCCESS_COLOR,
				false => CHECK_RESULT_FAILURE_COLOR,
			},
		}.to_egui();
		Frame::none()
			.outer_margin(Margin::same(4.0))
			.inner_margin(Margin::same(12.0))
			.stroke(Stroke::new(2.0, result_color))
			.show
			(
				ui,
				|ui|
				{
					ui.auto_sized_with_layout
					(
						Layout
						{
							main_dir: Direction::TopDown,
							..Layout::default()
						},
						|ui|
						{
							let spacing = ui.spacing_mut();
							spacing.item_spacing = Vec2::ZERO;
							ui.auto_sized_with_layout(Layout::left_to_right(Align::Center), |ui|
							{
								ui.label(label);
							});
							if check.keep_amt < dice.len()
							{
								ui.colored_label
								(
									CHECK_RESULT_DEFAULT_COLOR.to_egui(),
									format!
									(
										"Keep{} {}:",
										if check.keep_amt > 1 {format!(" {}", check.keep_amt)} else {String::default()},
										if check.keep_highest {"highest"} else {"lowest"},
									)
								);
							}
							ui.auto_sized_with_layout
							(
								Layout
								{
									main_dir: Direction::LeftToRight,
									cross_align: Align::Center,
									cross_justify: false,
									..Layout::default()
								},
								|ui|
								{
									ui.style_mut().override_text_style = Some(bevy_egui::egui::TextStyle::Name(DICIER_ANNOTATION_FONT_NAME.into()));
									let dice_render = check.dice_picks().into_iter().map(|(die, picked)|
									{
										let color = if picked {CHECK_RESULT_DEFAULT_COLOR} else {CHECK_RESULT_DISABLED_COLOR};
										(die, color.to_egui())
									});
									DiceWidget::new_with_colors(dice_render).ui(ui);
									ui.colored_label
									(
										CHECK_RESULT_DEFAULT_COLOR.to_egui(),
										format!
										(
											"{}{}",
											if check.modifier > 0 {"+"} else if check.modifier < 0 {"-"} else {""},
											if check.modifier != 0 {format!("{:0>2}", check.modifier.abs())} else {String::default()},
										)
									);
									if let Some((pledge_values, mut pledge_cancellations)) = pledges
									{
										if pledge_values.len() > 0
										{
											ui.colored_label(CHECK_RESULT_DEFAULT_COLOR.to_egui(), "+");
											DiceWidget::new_with_colors
												(
													pledge_values.into_iter()
														.map(|v| (*v as DieIntegral, PLEDGED_COLOR.to_egui()))
												)
												.with_clicked_iter_out(&mut pledge_cancellations)
												.ui(ui);
										}
									}
									ui.colored_label
									(
										result_color,
										format!
										(
											" = {:0>2}{}",
											check.total() + pledge_total as DieIntegral,
											match check.success_threshhold
											{
												None => String::default(),
												Some(threshhold) => format!(" vs {:0>2}", threshhold),
											},
										),
									);
									if let Some(boosting_state) = boosting_state
									{
										if let Some((text, color)) = match boosting_state
										{
											CheckPhase::NotBoosting => Some(("Boost", CHECK_RESULT_BOOST_COLOR)),
											CheckPhase::Boosting => Some(("Submit", CHECK_RESULT_SUBMIT_COLOR)),
											_ => None,
										}
										{
											ui.label(" ");
											if Button::new(RichText::new(text).color(CHECK_RESULT_BUTTON_TEXT_COLOR.to_egui()))
												.fill(color.to_egui())
												.ui(ui)
												.clicked()
											{
												match boosting_state
												{
													CheckPhase::NotBoosting => *boosting_state = CheckPhase::Boosting,
													CheckPhase::Boosting => *boosting_state = CheckPhase::Submitted,
													_ => {},
												}
											}
										}
									}
								},
							);
						},
					);
				}
			).response
	}
}