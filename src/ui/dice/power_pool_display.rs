use bevy_egui::egui;
use bevy_egui::egui::*;
use crate::*;

pub const ALLOCATION_COLOR: Color = Color::WHITE;
pub const PLEDGED_COLOR: Color = Color::GOLD;

pub trait PowerPoolDisplayExtension
{
	fn power_pool_display(&mut self, allocation: &mut LabeledPowerPool);
	fn allocations<'a, D: IntoIterator<Item = &'a mut Allocation>>(&mut self, dice: D);
}

impl PowerPoolDisplayExtension for Ui
{
	fn power_pool_display(&mut self, pool: &mut LabeledPowerPool)
	{
		let name = get_power_pool_name(pool.0);
		let color = get_power_pool_color(pool.0).to_egui();
		Frame::none()
			.outer_margin(Margin::same(4.0))
			.inner_margin(Margin::same(2.0))
			.stroke(Stroke::new(1.0, color))
			.show
			(
				self,
				|ui|
				{
					ui.with_layout
					(
						Layout
						{
							main_dir: egui::Direction::TopDown,
							..default()
						},
						|ui|
						{
							ui.colored_label(color, name);
							ui.allocations(pool.1.iter_mut());
						},
					);
				}
			);
	}

	fn allocations<'a, D: IntoIterator<Item = &'a mut Allocation>>(&mut self, allocations: D)
	{
		self.auto_sized_with_layout
		(
			Layout
			{
				main_dir: Direction::LeftToRight,
				..default()
			},
			|ui|
			{
				let allocations = allocations.into_iter().collect::<Vec<_>>();
				let dice_config = allocations.iter()
					.map(|allocation|
					{
						let color = if allocation.is_pledged {PLEDGED_COLOR} else {ALLOCATION_COLOR};
						(allocation.value as DieIntegral, color.to_egui())
					})
					.collect::<Vec<_>>();
				let mut clicked = allocations.iter().map(|_| false).collect::<Vec<_>>();
				DiceWidget::new_with_colors(dice_config.into_iter())
					.with_clicked_iter_out(clicked.as_mut_slice())
					.ui(ui);
				allocations.into_iter().zip(clicked.into_iter()).for_each(|(allocation, clicked)|
				{
					if clicked {allocation.is_pledged = !allocation.is_pledged;}
				});
			},
		);
	}
}