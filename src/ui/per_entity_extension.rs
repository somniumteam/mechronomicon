use bevy::ecs::entity::Entity;
use bevy_egui::egui::Window;

pub trait PerEntityExtension
{
	fn per_entity(self, entity: Entity) -> Self;
}
impl PerEntityExtension for Window<'_>
{
	fn per_entity(self, entity: Entity) -> Self
	{
		self.id(format!("{:?}", entity).into())
	}
}