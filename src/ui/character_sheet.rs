use bevy::prelude::*;
use bevy_egui::{egui, EguiContexts};
use crate::*;

pub struct CharacterSheetPlugin;
impl Plugin for CharacterSheetPlugin
{
	fn build(&self, app: &mut App)
	{
		app.add_systems(Update,
		(
			render_character_sheet,
		));
	}
}

pub fn render_character_sheet
(
	mut selected: Query
	<
		&mut PowerPools,
		With<IsSelected>
	>,
	mut egui: EguiContexts,
)
{
	if let Some(mut pools) = selected.get_single_mut().ok()
	{
		egui::Window::new("Character Sheet")
			.title_bar(true)
			.collapsible(true)
			.pivot(egui::Align2::LEFT_TOP)
			.default_pos(egui::Pos2::ZERO)
			.auto_sized()
			.show
			(
				egui.ctx_mut(),
				|ui|
				{
					ui.set_max_width(400.0);
					ui.with_layout
					(
						egui::Layout
						{
							main_dir: egui::Direction::TopDown,
							cross_align: egui::Align::Center,
							cross_justify: true,
							..default()
						},
						|ui|
						{
							for pool in pools.iter_mut()
							{
								ui.power_pool_display(pool);
							}
						},
					);
				}
			);
	}
}