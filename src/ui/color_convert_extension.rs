pub trait BevyColorToEguiColorExtension
{
	fn to_egui(&self) -> bevy_egui::egui::Color32;
}
impl BevyColorToEguiColorExtension for bevy::render::color::Color
{
	fn to_egui(&self) -> bevy_egui::egui::Color32
	{
		bevy_egui::egui::Color32::from_rgb
		(
			(self.r() * u8::MAX as f32).floor() as u8,
			(self.g() * u8::MAX as f32).floor() as u8,
			(self.b() * u8::MAX as f32).floor() as u8,
		)
	}
}