use bevy::prelude::*;
use bevy::app::AppExit;
use bevy::input::{keyboard::*, *};
use bevy_egui::egui::Widget;
use bevy_egui::*;
use crate::*;

pub struct MainMenuPlugin;
impl Plugin for MainMenuPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<ToggleMainMenuEvent>();
		app.add_event::<ToggleMainMenuEvent>();
		app.register_type::<MainMenuState>();
		app.init_resource::<MainMenuState>();
		app.add_systems(Startup, setup);
		app.add_systems
		(
			Update,
			(
				render_main_menu.before(clear_scenario),
				main_menu_hotkey.run_if(not(resource_exists::<IsEguiCapturingKeyboard>)),
				toggle_main_menu,
			)
		);
	}
}
fn setup
(
	mut main_menu_events: EventWriter<ToggleMainMenuEvent>,
)
{
	main_menu_events.send(ToggleMainMenuEvent::set(true));
}

#[derive(Resource, Default, Reflect)]
pub struct MainMenuState
{
	pub is_open: bool,
}

#[derive(Default, Event, Clone, Copy, Reflect)]
pub struct ToggleMainMenuEvent
{
	pub set_open: Option<bool>,
}
impl ToggleMainMenuEvent
{
	pub fn set(open: bool) -> Self {Self {set_open: Some(open)}}
	pub fn toggle() -> Self {Self::default()}
}

pub fn toggle_main_menu
(
	mut events: EventReader<ToggleMainMenuEvent>,
	mut state: ResMut<MainMenuState>,
)
{
	let mut should_open = state.is_open;
	for event in events.read()
	{
		should_open = event.set_open.unwrap_or(!should_open);
	}
	if state.is_open != should_open
	{
		state.is_open = should_open;
	}
}

pub fn main_menu_hotkey
(
	mut buttons: EventReader<KeyboardInput>,
	mut toggle: EventWriter<ToggleMainMenuEvent>,
)
{
	for button in buttons.read()
	{
		if button.state == ButtonState::Pressed && button.logical_key == Key::Escape
		{
			toggle.send(ToggleMainMenuEvent::toggle());
		}
	}
}

pub fn render_main_menu
(
	mut contexts: EguiContexts,
	mut state: ResMut<MainMenuState>,
	in_scenario: Query<(), With<ScenarioScope>>,
	mut clear_scenario: EventWriter<ClearScenario>,
	mut quick_play_launch: EventWriter<QuickPlayLaunch>,
	mut quick_versus_launch: EventWriter<QuickVersusLaunch>,
	mut open_options: EventWriter<OpenOptionsMenuEvent>,
	mut quit: EventWriter<AppExit>,
)
{
	if !state.is_open {return;}
	let ctx = contexts.ctx_mut();
	let mut should_close = false;
	egui::Window::new("Mechronomicon")
		.id("MainMenu".into())
		.anchor(egui::Align2::CENTER_CENTER, egui::Vec2::ZERO)
		.pivot(egui::Align2::CENTER_CENTER)
		.collapsible(false)
		.resizable(false)
		.show
		(
			ctx,
			|ui|
			{
				ui.auto_sized_with_layout
				(
					egui::Layout::top_down_justified(egui::Align::Center),
					|ui|
					{
						DiceWidget::new([1,2,3,4,5,6]).ui(ui);
						if !in_scenario.is_empty()
						{
							if ui.button("Resume").clicked()
							{
								should_close = true;
							}
						}
						if ui.button("Quick Play").clicked()
						{
							clear_scenario.send(ClearScenario::default());
							quick_play_launch.send(QuickPlayLaunch::default());
							should_close = true;
						}
						if ui.button("Quick Versus").clicked()
						{
							clear_scenario.send(ClearScenario::default());
							quick_versus_launch.send(QuickVersusLaunch::default());
							should_close = true;
						}
						if ui.button("Options").clicked()
						{
							open_options.send(default());
							should_close = true;
						}
						if cfg!(not(target_family="wasm"))
						{
							if ui.button("Quit").clicked()
							{
								quit.send(AppExit::default());
							}
						}
						DiceWidget::new([1,2,3,4,5,6]).ui(ui);
					},
				);
			}
		);
	if should_close {state.is_open = false;}
}