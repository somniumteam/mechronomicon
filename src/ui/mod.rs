use bevy::prelude::*;
use bevy_egui::{egui::{self, FontTweak}, EguiContexts};

pub mod main_menu;
pub use main_menu::*;
pub mod options_menu;
pub use options_menu::*;
pub mod dice;
pub use dice::*;
pub mod character_sheet;
pub use character_sheet::*;
pub mod phase;
pub use phase::*;
pub mod commands_ui;
pub use commands_ui::*;
pub mod egui_capture;
pub use egui_capture::*;
pub mod debug_overlay;
pub use debug_overlay::*;
pub mod color_convert_extension;
pub use color_convert_extension::*;
pub mod per_entity_extension;
pub use per_entity_extension::*;
pub mod auto_sized_with_layout_extension;
pub use auto_sized_with_layout_extension::*;

pub const MENU_BACKGROUND_COLOR: Color = Color::BLACK;
pub const MENU_BUTTON_IDLE_COLOR: Color = Color::DARK_GRAY;
pub const MENU_BUTTON_HOVER_COLOR: Color = Color::GRAY;
pub const MENU_BUTTON_PRESS_COLOR: Color = Color::MIDNIGHT_BLUE;

use include_bytes_plus::include_bytes;
pub const MENU_FONT_NAME: &str = "MenuFont";
pub const MENU_FONT: [u8; 394164] = include_bytes!("assets/Hack-Bold.ttf");
pub const DEFAULT_FONT_SIZE: f32 = 14.0;
pub const LARGE_FONT_SIZE: f32 = 20.0;
pub const DICIER_FONT_NAME: &str = "Dicier";
pub const DICIER_FONT: [u8; 548656] = include_bytes!("assets/Dicier-Block-Light.otf");
pub const DICIER_FONT_SIZE: f32 = 32.0;
pub const DICIER_ANNOTATION_FONT_NAME: &str = "DicierAnnotation";
pub const DICIER_ANNOTATION_FONT_SIZE: f32 = 24.0;

pub struct UIPlugin;
impl Plugin for UIPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<UISingletons>();
		app.add_plugins
		((
			DebugOverlayPlugin,
			EguiCapturePlugin,
			MainMenuPlugin,
			OptionsMenuPlugin,
			CharacterSheetPlugin,
			DiceUIPlugin,
			PhaseUIPlugin,
			OrdersUIPlugin,
		));
		app.add_systems(Startup, setup);
	}
}

#[derive(Resource, Clone, Reflect)]
pub struct UISingletons
{
	pub neon_logo: Handle<Image>,
	pub menu_font: Handle<Font>,
}

fn setup
(
	mut commands: Commands,
	asset_server: Res<AssetServer>,
	mut egui: EguiContexts,
)
{
	let singletons = UISingletons
	{
		neon_logo: asset_server.load("https://neon725.duckdns.org/Public/ChipFaceNewLowRez.png").into(),
		menu_font: asset_server.load("Hack-Bold.ttf").into(),
	};
	commands.insert_resource(singletons.clone());

	let mut fonts = egui::FontDefinitions::empty();
	fonts.font_data.insert
	(
		MENU_FONT_NAME.to_owned(),
		egui::FontData::from_static(&MENU_FONT),
	);
	fonts.font_data.insert
	(
		DICIER_FONT_NAME.to_owned(),
		egui::FontData::from_static(&DICIER_FONT).tweak(FontTweak
		{
			y_offset_factor: 0.15,
			..default()
		}),
	);
	for family in vec![egui::FontFamily::Proportional, egui::FontFamily::Monospace]
	{
		fonts.families
			.entry(family)
			.or_default()
			.push(MENU_FONT_NAME.to_owned());
	}
	fonts.families
		.entry(egui::FontFamily::Name(DICIER_FONT_NAME.into()))
		.or_default()
		.push(DICIER_FONT_NAME.to_owned());
	let ctx = egui.ctx_mut();
	ctx.set_fonts(fonts);
	ctx.style_mut(|style|
	{
		style.text_styles =
		[
			(egui::TextStyle::Heading, egui::FontId::new(LARGE_FONT_SIZE, egui::FontFamily::Proportional)),
			(egui::TextStyle::Body, egui::FontId::new(DEFAULT_FONT_SIZE, egui::FontFamily::Proportional)),
			(egui::TextStyle::Button, egui::FontId::new(LARGE_FONT_SIZE, egui::FontFamily::Proportional)),
			(egui::TextStyle::Name(DICIER_FONT_NAME.into()), egui::FontId::new(DICIER_FONT_SIZE, egui::FontFamily::Name(DICIER_FONT_NAME.into()))),
			(egui::TextStyle::Name(DICIER_ANNOTATION_FONT_NAME.into()), egui::FontId::new(DICIER_ANNOTATION_FONT_SIZE, egui::FontFamily::Monospace)),
		].into();
	});
}