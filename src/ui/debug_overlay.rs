use bevy::prelude::*;
use bevy::input::ButtonState;
use bevy::input::keyboard::KeyboardInput;
use bevy::window::PrimaryWindow;
use bevy_egui::*;
use bevy_inspector_egui::*;

pub struct DebugOverlayPlugin;
impl Plugin for DebugOverlayPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<DebugGuiOpen>();
		app.init_resource::<DebugGuiOpen>();
		app.add_plugins
		((
			EguiPlugin,
			DefaultInspectorConfigPlugin,
		));
		app.add_systems(Update, toggle_debug_gui);
		app.add_systems(Last, update_debug_gui);
	}
}

#[derive(Default, Resource, Debug, Reflect, Copy, Clone, PartialEq, Eq)]
struct DebugGuiOpen(bool);

fn toggle_debug_gui
(
	mut gui_open: ResMut<DebugGuiOpen>,
	mut presses: EventReader<KeyboardInput>,
)
{
	for press in presses.read()
	{
		if press.key_code == KeyCode::Backquote && press.state == ButtonState::Released
		{
			let gui_open = gui_open.as_mut();
			gui_open.0 = !gui_open.0;
		}
	}
}

fn update_debug_gui(world: &mut World)
{
    let gui_open = world.resource::<DebugGuiOpen>();
    if !gui_open.0 {return;}

    let Ok(egui_context) = world
        .query_filtered::<&mut EguiContext, With<PrimaryWindow>>()
        .get_single(world)
    else {return;};

    let mut egui_context = egui_context.clone();
    let context = egui_context.get_mut();

    egui::Window::new("DEBUG_GUI").show(&context, |ui|
	{
        egui::ScrollArea::vertical().show(ui, |ui|
		{
            bevy_inspector::ui_for_world(world, ui);
        });
    });
}