use bevy::prelude::*;
use bevy_egui::{egui, EguiContexts};

use crate::{IsLocalPlayer, MainMenuState, ToggleMainMenuEvent};

pub struct OptionsMenuPlugin;
impl Plugin for OptionsMenuPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<IsOptionsMenuOpen>();
		app.register_type::<OpenOptionsMenuEvent>();
		app.add_event::<OpenOptionsMenuEvent>();
		app.add_systems(Update, options_menu);
	}
}

#[derive(Resource, Default, Reflect)]
pub struct IsOptionsMenuOpen;

#[derive(Event, Default, Reflect)]
pub struct OpenOptionsMenuEvent;

pub fn options_menu
(
	mut commands: Commands,
	mut events: EventReader<OpenOptionsMenuEvent>,
	is_open: Option<Res<IsOptionsMenuOpen>>,
	main_menu_state: Res<MainMenuState>,
	mut main_menu_toggle: EventWriter<ToggleMainMenuEvent>,
	mut egui: EguiContexts,
	mut name: Query<&mut Name, With<IsLocalPlayer>>,
)
{
	if !events.is_empty()
	{
		events.clear();
		commands.init_resource::<IsOptionsMenuOpen>();
	}
	if is_open.is_some()
	{
		if main_menu_state.is_open
		{
			commands.remove_resource::<IsOptionsMenuOpen>();
			return;
		}
		egui::Window::new("Options")
			.anchor(egui::Align2::CENTER_CENTER, egui::Vec2::ZERO)
			.pivot(egui::Align2::CENTER_CENTER)
			.collapsible(false)
			.resizable(false)
			.min_width(400.)
			.show(egui.ctx_mut(), |ui|
			{
				if ui.button("Back").clicked()
				{
					main_menu_toggle.send(ToggleMainMenuEvent::set(true));
					commands.remove_resource::<IsOptionsMenuOpen>();
				}
				let mut name = name.get_single_mut().unwrap();
				let mut name_str_mut = name.to_string();
				let response = ui.text_edit_singleline(&mut name_str_mut);
				if response.changed()
				{
					*name = Name::new(name_str_mut);
				}
			});
	}
}