use bevy_egui::egui::{self, InnerResponse};

pub trait AutoSizedWithLayoutExtension
{
	fn auto_sized_with_layout<R>(&mut self, layout: egui::Layout, add_contents: impl FnOnce(&mut Self) -> R) -> InnerResponse<R>;
}
impl AutoSizedWithLayoutExtension for egui::Ui
{
	fn auto_sized_with_layout<R>(&mut self, layout: egui::Layout, add_contents: impl FnOnce(&mut Self) -> R) -> InnerResponse<R>
	{
		self.allocate_ui_with_layout
		(
			egui::Vec2::default(),
			layout,
			add_contents
		)
	}
}