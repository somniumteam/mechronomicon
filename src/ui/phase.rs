use bevy::prelude::*;
use bevy_egui::{egui, EguiContexts};
use crate::*;

pub struct PhaseUIPlugin;
impl Plugin for PhaseUIPlugin
{
	fn build(&self, app: &mut App)
	{
		app.add_systems(Update,
			phase_display.run_if(resource_exists::<InScenario>)
		);
	}
}

pub fn phase_display
(
	mut egui: EguiContexts,
	phase: Res<State<GamePhase>>,
	player_names: Query<&Name, With<Player>>,
	local_player: Query
	<
		(
			Entity,
			Option<&IsPlanning>,
			Option<&SubmittedPlan>,
		),
		With<IsLocalPlayer>,
	>,
	initiative: Option<Res<InitiativeOrder>>,
	planning_order_index: Option<Res<PlanningOrderIndex>>,
	mut plan_submission: EventWriter<SubmitPlanEvent>,
)
{
	let
	(
		local_player,
		local_player_planning,
		local_player_submitted,
	) = local_player.get_single().unwrap();
	let local_player_planning = local_player_planning.is_some();
	let local_player_submitted = local_player_submitted.is_some();
	let planning_order_index = planning_order_index
		.map(|oi| oi.0)
		.unwrap_or(0);
	let phase = phase.get().to_owned();
	let ctx = egui.ctx_mut();
	egui::Window::new(format!("{}", phase))
		.id("PhaseDisplay".into())
		.pivot(egui::Align2::RIGHT_TOP)
		.anchor(egui::Align2::RIGHT_TOP, egui::Vec2::ZERO)
		.resizable(egui::Vec2b::FALSE)
		.movable(false)
		.collapsible(false)
		.show(ctx, |ui|
		{
			match phase
			{
				GamePhase::PLANNING =>
				{
					ui.auto_sized_with_layout
					(
						egui::Layout::top_down(egui::Align::LEFT),
						|ui|
						{
							for (i, faction) in initiative.unwrap().0.iter().enumerate()
							{
								let is_planning = i == planning_order_index;
								let planning_indicator = if is_planning {">"}
								else {""};
								ui.auto_sized_with_layout
								(
									egui::Layout::left_to_right(egui::Align::TOP),
									|ui| ui.heading(format!("{planning_indicator}{}", faction.name)),
								);
								if faction.members.len() > 1
								{
									for &member in faction.members.iter()
									{
										let name = player_names.get(member).unwrap();
										ui.auto_sized_with_layout
										(
											egui::Layout::left_to_right(egui::Align::TOP),
											|ui| ui.label(format!("{}", name)),
										);
									}
								}
							}
							if local_player_planning && !local_player_submitted
							{
								if ui.button("Submit Orders").clicked()
								{
									plan_submission.send(SubmitPlanEvent
									{
										player: local_player,
									});
								}
							}
						},
					);
				},
				_ => {},
			}
		});
}