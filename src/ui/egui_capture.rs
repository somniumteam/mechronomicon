use bevy::prelude::*;
use bevy_inspector_egui::bevy_egui::EguiContextQuery;

pub struct EguiCapturePlugin;
impl Plugin for EguiCapturePlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<IsEguiCapturingMouse>();
		app.register_type::<IsEguiCapturingKeyboard>();
		app.add_systems(Update, update_egui_capture_state);
	}
}

#[derive(Resource, Default, Reflect)]
pub struct IsEguiCapturingMouse;

#[derive(Resource, Default, Reflect)]
pub struct IsEguiCapturingKeyboard;

pub fn update_egui_capture_state
(
	mut commands: Commands,
	mouse_state: Option<Res<IsEguiCapturingMouse>>,
	keyboard_state: Option<Res<IsEguiCapturingKeyboard>>,
	mut egui_context: Query<EguiContextQuery>,
)
{
	let (mouse, keyboard) = egui_context.get_single_mut().map(|mut c|
	{
		let ctx = c.ctx.get_mut();
		(ctx.wants_pointer_input(), ctx.wants_keyboard_input())
	}).unwrap_or((false, false));
	if mouse != mouse_state.is_some()
	{
		if mouse {commands.init_resource::<IsEguiCapturingMouse>();}
		else {commands.remove_resource::<IsEguiCapturingMouse>();}
	}
	if keyboard != keyboard_state.is_some()
	{
		if keyboard {commands.init_resource::<IsEguiCapturingKeyboard>();}
		else {commands.remove_resource::<IsEguiCapturingKeyboard>();}
	}
}