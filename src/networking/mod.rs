use bevy::prelude::*;
use bevy_matchbox::prelude::*;
use crate::*;

pub struct NetworkingPlugin;
impl Plugin for NetworkingPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<NetworkRole>();
		app.register_type::<NetworkConnectEvent>();
		app.add_event::<NetworkConnectEvent>();
		app.init_state::<NetworkRole>();
		app.add_systems(Update,
		(
			socket_open,
			socket_listen.run_if(resource_exists::<MatchboxSocket<SingleChannel>>),
		));
	}
}

#[derive(States, Reflect, Default, Debug, Copy, Clone, Hash, PartialEq, Eq)]
pub enum NetworkRole
{
	#[default]
	Authoritative,
	NonAuthoritative,
}

#[derive(Event, Reflect, Default)]
pub struct NetworkConnectEvent
{
	pub role: NetworkRole,
	pub extra_args: Option<String>,
	pub host: Option<String>,
}

fn socket_open
(
	mut commands: Commands,
	mut connect_events: EventReader<NetworkConnectEvent>,
	mut network_state: ResMut<NextState<NetworkRole>>,
)
{
	for event in connect_events.read()
	{
		let host = event.host.to_owned()
			.unwrap_or("neon725.duckdns.org".into());
		let extra = event.extra_args.to_owned()
			.map(|s| format!("?{s}")).unwrap_or("".into());
		let room_url = format!("ws://{host}:3536/mechronomicon{extra}");
		info!("Connecting to matchbox server: {room_url}");
		commands.insert_resource(MatchboxSocket::new_unreliable(room_url));
		network_state.set(event.role);
	}
}

fn socket_listen
(
	mut commands: Commands,
	mut socket: ResMut<MatchboxSocket<SingleChannel>>,
	players: Query<(Entity, &Player)>,
	mut scenario_clear: EventWriter<ClearScenario>,
	mut main_menu: EventWriter<ToggleMainMenuEvent>,
)
{
	match socket.try_update_peers()
	{
		Ok(updates) =>
		{
			for (peer_id, peer_state) in updates.into_iter()
			{
				let existing_player = players.iter()
					.find(|(_, p)|
						p.peer_id.is_some_and(|id| id == peer_id)
					);
				match peer_state
				{
					PeerState::Connected =>
					{
						//TODO: UI Log (#16)
						info!("Peer connected: {:?}", peer_id);
						if existing_player.is_none()
						{
							commands.spawn(PlayerBundle::new("Connecting Player"));
						}
					},
					PeerState::Disconnected =>
					{
						//TODO: UI Log (#16)
						info!("Peer disconnected: {:?}", peer_id);
						if let Some((player, ..)) = existing_player
						{
							commands.entity(player).despawn_recursive();
						}
					},
				}
			}
		},
		Err(_) =>
		{
			//TODO: UI Log (#16)
			commands.remove_resource::<MatchboxSocket<SingleChannel>>();
			scenario_clear.send(ClearScenario);
			main_menu.send(ToggleMainMenuEvent::set(true));
		}
	}
}