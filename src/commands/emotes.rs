use bevy::prelude::*;
use std::time::Duration;
use crate::*;

pub const EMOTE_INTERPOLATION_DISTANCE: f32 = 75.;
pub const EMOTE_INTERPOLATION_TIME: Duration = Duration::from_millis(1800);
pub const EMOTE_INTERPOLATION_DIRECTION: Vec2 = Vec2::new(0.0, 1.0);
pub const EMOTE_DEPTH_OFFSET: f32 = 1.;
pub const EMOTE_HEIGHT: f32 = 20.;

pub struct EmotesPlugin;
impl Plugin for EmotesPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<EmoteInterpolation>();
		app.add_systems(Startup, setup);
		app.add_systems(Update, interpolate_emotes);
		app.world.register_action_resolution_system(ActorCommandType::Emote, resolve_emote_action);
	}
}
fn setup
(
	mut commands: Commands,
	asset_server: Res<AssetServer>,
)
{
	commands.insert_resource(EmoteSingletons
	{
		emote_image: asset_server.load("face-with-tongue-emoji-icon.png"),
	});
}

#[derive(Resource)]
pub struct EmoteSingletons
{
	pub emote_image: Handle<Image>,
}

pub fn resolve_emote_action
(
	host: In<ActorCommandSystemParams>,
	mut commands: Commands,
	singletons: Res<EmoteSingletons>,
	images: Res<Assets<Image>>,
	position: Query<&Transform>,
) -> bool
{
	let raw_height = images.get(singletons.emote_image.clone()).unwrap().size_f32().y;
	let scale = Vec3::splat(EMOTE_HEIGHT / raw_height);
	let (entity, ..) = *host;
	let start = position.get(entity).unwrap().to_owned().translation + Vec2::ZERO.extend(EMOTE_DEPTH_OFFSET);
	commands.spawn
	((
		EmoteInterpolation
		{
			start,
			..default()
		},
		SpriteBundle
		{
			texture: singletons.emote_image.clone(),
			transform: Transform::from_scale(scale)
				.with_translation(start),
			..default()
		},
		Name::new("Emote"),
	));
	true
}

#[derive(Component, Default, Debug, Reflect)]
pub struct EmoteInterpolation
{
	start: Vec3,
	progress: Duration,
}

pub fn interpolate_emotes
(
	mut commands: Commands,
	time: Res<Time>,
	mut emotes: Query<(Entity, &mut EmoteInterpolation, &mut Transform)>,
)
{
	for (entity, mut interpolation, mut transform) in emotes.iter_mut()
	{
		interpolation.progress += time.delta();
		let ratio = interpolation.progress.div_duration_f32(EMOTE_INTERPOLATION_TIME);
		if ratio <= 1.
		{
			let dist = ratio * EMOTE_INTERPOLATION_DISTANCE;
			transform.translation = interpolation.start + EMOTE_INTERPOLATION_DIRECTION.extend(0.) * dist;
		}
		else
		{
			commands.entity(entity).despawn_recursive();
		}
	}
}