use bevy::prelude::*;
use crate::*;

pub struct MoveCommandPlugin;
impl Plugin for MoveCommandPlugin
{
	fn build(&self, app: &mut App)
	{
		app.world.register_action_resolution_system(ActorCommandType::Move, resolve_move_command);
	}
}

pub fn resolve_move_command
(
	input: In<ActorCommandSystemParams>,
	mut commands: Commands,
	host_components: Query
	<(
		&HexCoordinate,
		Option<&PathfindingRequest>,
		Option<&PathfindingInterpolation>,
		Ref<Transform>,
	)>,
) -> bool
{
	let (host, params) = *input;
	let destination = match params
	{
		ActorCommandParams::Hex { hex } => hex,
		p => panic!("Incorrect param type for Move: {:?}", p),
	};
	let
	(
		current_hex,
		request,
		interpolation,
		transform,
	) = host_components.get(host).unwrap();
	if let Some(request) = request
	{
		if let Some(result) = request.result()
		{
			if let Ok(result) = result
			{
				commands.entity(host)
					.insert(PathfindingInterpolation
					{
						remaining_positions: result.to_owned(),
					})
					.remove::<PathfindingRequest>();
				false
			}
			else
			{
				// TODO: Handle failures. (#22)
				true
			}
		}
		else {false}
	}
	else if let Some(interpolation) = interpolation
	{
		if !transform.is_changed()
		{
			if !interpolation.remaining_positions.is_empty()
			{
				let mut new_interpolation = interpolation.to_owned();
				let next = new_interpolation.remaining_positions.remove(0);
				commands.entity(host)
					.insert(next)
					.insert(new_interpolation);
				false
			}
			else {true}
		}
		else {false}
	}
	else
	{
		commands.entity(host).insert(PathfindingRequest::new(*current_hex, destination));
		false
	}
}