use bevy::prelude::*;
use bevy::utils::HashMap;
use bevy::ecs::system::SystemId;
use crate::*;

pub mod emotes;
pub use emotes::*;
pub mod move_command;
pub use move_command::*;

pub struct CommandsPlugin;
impl Plugin for CommandsPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<ActorCommand>();
		app.register_type::<ActorCommandType>();
		app.register_type::<ActorCommandParams>();
		app.add_plugins(EmotesPlugin);
		app.add_plugins(MoveCommandPlugin);
	}
}

pub type ActorCommandSystemParams = (Entity, ActorCommandParams);
pub type ActorCommandSystemId = SystemId<ActorCommandSystemParams, bool>;

#[derive(Default, Resource)]
pub struct ActorCommandSystemLibrary
{
	library: HashMap<ActorCommandType, ActorCommandSystemId>,
}
impl ActorCommandSystemLibrary
{
	pub fn get(&self, command_type: ActorCommandType) -> ActorCommandSystemId
	{
		self.library.get(&command_type)
			.expect(format!("No system registered to resolve {:?}", command_type).as_str())
			.to_owned()
	}
}

pub trait ActionResolutionWorldExtension
{
	fn register_action_resolution_system
	<
		Marker,
		S: IntoSystem<ActorCommandSystemParams, bool, Marker>,
	>
	(
		&mut self,
		command_type: ActorCommandType,
		system: S
	);

	fn run_action_resolution
	(
		&mut self,
		host: Entity,
		command_type: ActorCommandType,
		params: ActorCommandParams,
	) -> bool;
}
impl ActionResolutionWorldExtension for World
{
	fn register_action_resolution_system
	<
		Marker,
		S: IntoSystem<ActorCommandSystemParams, bool, Marker>,
	>
	(
		&mut self,
		command_type: ActorCommandType,
		system: S
	)
	{
		let system_id = self.register_system(IntoSystem::into_system(system));
		self.init_resource::<ActorCommandSystemLibrary>();
		let mut library = self.get_resource_mut::<ActorCommandSystemLibrary>().unwrap();
		library.library.insert(command_type, system_id);
	}

	fn run_action_resolution
	(
		&mut self,
		host: Entity,
		command_type: ActorCommandType,
		params: ActorCommandParams,
	) -> bool
	{
		let system = self.get_resource::<ActorCommandSystemLibrary>()
			.unwrap()
			.get(command_type);
		self.run_system_with_input(system, (host, params)).unwrap()
	}
}

#[derive(Reflect, Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum ActorCommandType
{
	Emote,
	Move,
}
#[derive(Reflect, Clone, Copy, Debug, Default)]
pub enum ActorCommandParams
{
	#[default]
	None,
	Hex { hex: HexCoordinate },
}
#[derive(Reflect, Clone, Copy, Debug)]
pub struct ActorCommand
{
	pub command_type: ActorCommandType,
	pub params: ActorCommandParams,
	pub phase: ResolutionPhase,
}
impl ActorCommand
{
	pub fn new(command_type: ActorCommandType, params: ActorCommandParams) -> Self
	{
		Self
		{
			command_type,
			params,
			phase: match command_type
			{
				ActorCommandType::Emote => ResolutionPhase::SELF,
				ActorCommandType::Move => ResolutionPhase::FINALIZE,
			},
		}
	}
}