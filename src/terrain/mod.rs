use std::collections::HashMap;
use bevy::prelude::*;
use bevy::ecs::system::*;
use grid::Grid;
use crate::*;

pub mod tile_entity;
pub use tile_entity::*;
pub mod pathfinding;
pub use pathfinding::*;

pub struct TerrainPlugin;
impl Plugin for TerrainPlugin
{
	fn build(&self, app: &mut App)
	{
		app.add_plugins(PathfindingPlugin);
		app.add_systems(Startup, setup);
	}
}

#[derive(Resource)]
pub struct TerrainSingletons
{
	pub default_tile_pallette: HashMap<String, TilePalletteEntry>,
}
pub struct TilePalletteEntry
{
	pub scale: Vec2,
	pub image: Handle<Image>,
}

fn setup
(
	mut commands: Commands,
	asset_server: Res<AssetServer>,
	hex_spacing: Res<HexSpacing>,
)
{
	let mut default_tile_pallette = HashMap::new();
	default_tile_pallette.insert("blank".into(), TilePalletteEntry
	{
		image: asset_server.load("Hexagon Blank.png"),
		scale: Vec2::splat(hex_spacing.0 / 55.0),
	});
	default_tile_pallette.insert("grass".into(), TilePalletteEntry
	{
		image: asset_server.load("Hexagon Grass.png"),
		scale: Vec2::splat(hex_spacing.0 / 55.0),
	});
	commands.insert_resource(TerrainSingletons
	{
		default_tile_pallette,
	});
}

pub type TerrainGridEntry = (TerrainTileState, Option<Entity>);

#[derive(Resource)]
pub struct TerrainState
{
	grid: Grid<TerrainGridEntry>,
	origin: HexCoordinate,
}
impl TerrainState
{
	pub fn new(size: usize) -> Self
	{
		Self
		{
			grid: Grid::new(size, size),
			origin: HexCoordinate{q: (size / 2) as HexIntegral, r: (size / 2) as HexIntegral},
		}
	}

	pub fn get(self: &Self, hex: HexCoordinate) -> Option<&TerrainGridEntry>
	{
		let hex = (hex + self.origin).round();
		self.grid.get(hex.r as usize, hex.q as usize)
	}

	pub fn get_mut(self: &mut Self, hex: HexCoordinate) -> Option<&mut TerrainGridEntry>
	{
		let hex = (hex + self.origin).round();
		self.grid.get_mut(hex.r as usize, hex.q as usize)
	}
}

pub struct TerrainTileState
{
	pub height: u8,
	pub base_type: String,
}
impl Default for TerrainTileState
{
	fn default() -> Self
	{
		Self
		{
			height: 0,
			base_type: "blank".into(),
		}
	}
}
impl TerrainTileState
{
	pub fn get_tile_entity_bundle(&self, singletons: &TerrainSingletons) -> TileEntityBundle
	{
		TileEntityBundle::new(singletons, self.base_type.to_owned())
			.with_name(format!("{} Tile", self.base_type.to_owned()))
	}
}

struct ModifyTerrainCommand<BundleT: Bundle>
{
	hex: HexCoordinate,
	new_state: TerrainTileState,
	additional: Option<BundleT>,
}
impl<T: Bundle> Command for ModifyTerrainCommand<T>
{
	fn apply(self, world: &mut World)
	{
		/*
			Code flow is scuffed because entities cannot be modified while the resources are in scope.
			Tile data is precomputed as much as possible, then entities are spawned/despawned.
			Bidirectionality happens at the last step after entities are spawned, which involves
			reacquiring the terrain grid resource.
		*/
		let (existing_entity, new_bundle) =
		{
			let world = world.cell();
			let mut terrain = world.resource_mut::<TerrainState>();
			let (tile, existing_entity) = terrain.get_mut(self.hex).ok_or("Tried to modify terrain tile beyond maximum range.").unwrap();
			*tile = self.new_state;
			let bundle = if tile.base_type != "blank"
			{
				let singletons = world.resource::<TerrainSingletons>();
				let mut bundle = tile.get_tile_entity_bundle(&singletons);
				bundle.hex = self.hex;
				Some(bundle)
			}
			else {None};
			(*existing_entity, bundle)
		};
		if let Some(existing_entity) = existing_entity {world.entity_mut(existing_entity).despawn_recursive();}
		let new_entity = if self.additional.is_some() || new_bundle.is_some()
		{
			let mut entity = world.spawn_empty();
			if let Some(bundle) = new_bundle {entity.insert(bundle);}
			if let Some(bundle) = self.additional {entity.insert(bundle);}
			Some(entity.id())
		}
		else {None};
		world.resource_mut::<TerrainState>().get_mut(self.hex).unwrap().1 = new_entity;
	}
}
pub trait ModifyTerrainExt
{
	fn modify_terrain(&mut self, hex: HexCoordinate, state: TerrainTileState);
	fn modify_terrain_ext<T: Bundle>(&mut self, hex: HexCoordinate, state: TerrainTileState, additional: T);
}
impl<'w, 's> ModifyTerrainExt for Commands<'w, 's>
{
	fn modify_terrain(&mut self, hex: HexCoordinate, state: TerrainTileState)
	{
		self.add(ModifyTerrainCommand::<Transform>
		{
			hex,
			new_state: state,
			additional: None,
		});
	}
	fn modify_terrain_ext<T: Bundle>(&mut self, hex: HexCoordinate, state: TerrainTileState, additional: T)
	{

		self.add(ModifyTerrainCommand
		{
			hex,
			new_state: state,
			additional: Some(additional),
		});
	}
}