use std::time::Duration;
use bevy::prelude::*;
use bevy::time::common_conditions::on_timer;
use ::pathfinding::prelude::astar;
use crate::*;

pub struct PathfindingPlugin;
impl Plugin for PathfindingPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<PathfindingRequest>();
		app.add_systems(Update,
		(
			process_pathfinding_requests.run_if(on_timer(Duration::from_millis(100))),
		));
	}
}

pub const PATHFINDING_MAXIMUM_DEPTH: usize = 1024;

#[derive(Component, Reflect)]
pub struct PathfindingRequest
{
	start: HexCoordinate,
	end: HexCoordinate,
	result: Option<Result<Vec<HexCoordinate>, String>>,
}
impl PathfindingRequest
{
	pub fn new(start: HexCoordinate, end: HexCoordinate) -> Self
	{
		Self
		{
			start,
			end,
			result: None,
		}
	}

	pub fn from_template(other: &PathfindingRequest) -> Self
	{
		Self::new(other.start, other.end)
	}

	pub fn matches_template(&self, other: &PathfindingRequest) -> bool
	{
		self.start == other.start && self.end == other.end
	}

	pub fn result(&self) -> Option<Result<Vec<HexCoordinate>, String>>
	{
		self.result.clone()
	}
}

pub fn process_pathfinding_requests
(
	mut requests: Query<&mut PathfindingRequest>,
	terrain: Option<Res<TerrainState>>,
)
{
	if terrain.is_none() {return;}
	let terrain = terrain.unwrap();
	for mut request in requests.iter_mut()
	{
		if request.result.is_none()
		{
			let PathfindingRequest
			{
				ref start,
				ref end,
				ref mut result,
				..
			} = *request;
			let path = astar
			(
				start,
				|&h|
				{
					HexCoordinate::DIRECTIONS.to_vec().iter()
						.map(|d| (h + *d))
						.filter
						(
							|h| terrain
								.get(*h)
								.map(|t| t.0.base_type != "blank")
								.unwrap_or(false)
						)
						.map(|h| (h, 1))
						.collect::<Vec<_>>()
				},
				|h| h.manhattan_distance(*end).floor() as u32,
				|h| *h == *end,

			);
			*result = Some
			(
				path
					.map(|(p, _)| p.into_iter().skip(1).collect())
					.ok_or("No valid path.".into())
			);
		}
	}
}