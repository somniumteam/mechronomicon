use std::borrow::Cow;
use bevy::prelude::*;
use crate::*;

#[derive(Bundle)]
pub struct TileEntityBundle
{
	pub hex: HexCoordinate,
	pub sprite: SpriteBundle,
	pub scenario: ScenarioScope,
	pub name: Name,
}
impl Default for TileEntityBundle
{
	fn default() -> Self
	{
		Self
		{
			hex: default(),
			sprite: default(),
			scenario: default(),
			name: Name::new("Tile"),
		}
	}
}
impl TileEntityBundle
{
	pub fn new
	(
		singletons: &TerrainSingletons,
		base_type: String,
	) -> Self
	{
		let pallette_entry = singletons.default_tile_pallette.get(&base_type).unwrap();
		Self
		{
			sprite: SpriteBundle
			{
				texture: pallette_entry.image.clone(),
				transform: Transform::from_scale(pallette_entry.scale.extend(1.0)),
				..default()
			},
			..default()
		}
	}

	pub fn with_name(mut self, name: impl Into<Cow<'static, str>>) -> Self
	{
		self.name.set(name);
		self
	}
}