use bevy::prelude::*;
use bevy::ecs::system::EntityCommands;
use itertools::Itertools;
use crate::*;

pub struct ContestedCheckPlugin;
impl Plugin for ContestedCheckPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<ContestFactionInfo>();
		app.register_type::<ContestedCheck>();
		app.add_systems(Update,
			process_contested_checks
				.after(roll_pending_checks)
				.after(check_submissions)
		);
	}
}

#[derive(Reflect, Clone)]
pub struct ContestFactionInfo
{
	pub name: Name,
	pub members: Vec<Entity>,
}
impl ContestFactionInfo
{
	pub fn new<N: Into<Name>, P: IntoIterator<Item = Entity>>(name: N, members: P) -> Self
	{
		Self
		{
			name: name.into(),
			members: members.into_iter().collect(),
		}
	}
}

#[derive(Component, Default, Reflect)]
pub struct ContestedCheck
{
	pub competing_factions: Vec<ContestFactionInfo>,
	pub finished_factions: Vec<ContestFactionInfo>,
}
#[derive(Bundle)]
pub struct ContestedCheckBundle
{
	contest: ContestedCheck,
	name: Name,
	scope: ScenarioScope,
}

pub trait ContestedCheckExtension
{
    fn create_contested_check
	<
		P: IntoIterator<Item = Entity>,
		F: IntoIterator<Item = (Name, P)>,
		N: Into<Name>,
	>
	(
		&mut self,
		label: N,
		definition: &Check,
		factions: F,
	) -> EntityCommands;
}

impl<'w, 's> ContestedCheckExtension for Commands<'w, 's>
{
    fn create_contested_check
	<
		P: IntoIterator<Item = Entity>,
		F: IntoIterator<Item = (Name, P)>,
		N: Into<Name>,
	>
	(
		&mut self,
		label: N,
		check: &Check,
		factions: F,
	) -> EntityCommands
	{
		let factions = factions.into_iter()
			.map(|(n, f)| ContestFactionInfo::new(n, f))
			.collect::<Vec<_>>();
		let names = factions.iter()
			.map(|info| info.name.clone())
			.collect::<Vec<_>>();
		let bundle = ContestedCheckBundle
		{
			contest: ContestedCheck
			{
				competing_factions: factions,
				..Default::default()
			},
			name: label.into(),
			scope: default(),
		};
		let mut check = check.clone();
		check.responsible_players.clear();
		let mut commands = self.spawn(bundle);
		commands.with_children(move |builder|
		{
			for name in names
			{
				builder.spawn(PendingCheckBundle::new(name, check.clone()));
			}
		});
		commands
	}
}

pub fn process_contested_checks
(
	mut contests: Query<(Entity, &mut ContestedCheck)>,
	mut checks: Query<(&Parent, &mut Check)>,
)
{
	for (contest_entity, mut contest) in contests.iter_mut()
	{
		let mut checks = checks.iter_mut()
			.filter(|(parent, ..)| parent.get() == contest_entity)
			.map(|(_, result)| result)
			.collect::<Vec<_>>();
		let mut active_faction_checks = contest.competing_factions.iter_mut().zip(checks.iter_mut())
			.enumerate()
			.map(|(original_index, (faction, check))|
			{
				let total = check.total();
				let by_dice = check.kept_dice().sum::<DieIntegral>();
				(faction, check, original_index, total, by_dice)
			})
			.sorted_by(|(_, _, _, a_total, a_dice), (_, _, _, b_total, b_dice)|
				match a_total.cmp(&b_total)
				{
					std::cmp::Ordering::Equal => a_dice.cmp(&b_dice),
					other => other,
				}
			)
			.map(|(faction, check, original_index, total, ..)| (faction, check, original_index, total))
			.collect::<Vec<_>>();
		if active_faction_checks.is_empty() {continue;}
		let promote = if active_faction_checks.len() == 1
		{
			Some(0)
		}
		else
		{
			let maintain_threshhold = active_faction_checks.get(1).unwrap().3;
			let pass_threshhold = maintain_threshhold + 1;
			let mut should_promote = None;
			for (i, (faction, check, original_index, ..)) in active_faction_checks.iter_mut().enumerate()
			{
				let retval = if i == 0
				{
					if
						check.success_threshhold.is_none()
						|| check.success_threshhold.unwrap() != pass_threshhold
					{
						check.success_threshhold = Some(pass_threshhold);
					}
					if check.responsible_players.is_empty()
					{
						check.responsible_players.extend(faction.members.iter());
					}
					if check.phase == CheckPhase::Submitted
					{
						/*
							NOTE: Submitting causes totals to update, which recalculates worst_faction.
							The only way worst_faction can be submitted is if the player(s) did not meet
							the pass threshhold.
						*/
						check.responsible_players.clear();
						Some(*original_index)
					}
					else {None}
				}
				else
				{
					if !check.responsible_players.is_empty()
					{
						check.responsible_players.clear();
					}
					if
					check.success_threshhold.is_none()
						|| check.success_threshhold.unwrap() != maintain_threshhold
					{
						check.success_threshhold = Some(maintain_threshhold);
					}
					None
				};
				if retval.is_some()
				{
					should_promote = retval;
					break;
				}
			}
			should_promote
		};
		if let Some(i) = promote
		{
			let removed = contest.competing_factions.remove(i);
			contest.finished_factions.push(removed);
		}
	}
}