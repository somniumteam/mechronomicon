use std::cmp::Ordering;
use bevy::prelude::*;
use bevy_egui::egui::{self, Widget};
use rand::Rng;
use crate::*;

pub mod contested;
pub use contested::*;

pub type DieIntegral = i8;
pub const DIE_MINIMUM: DieIntegral = 1;
pub const DIE_MAXIMUM: DieIntegral = 6;

pub struct DicePlugin;
impl Plugin for DicePlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<Check>();
		app.register_type::<CheckPhase>();
		app.register_type::<SubmitCheckEvent>();
		app.add_event::<SubmitCheckEvent>();
		app.add_systems(Update,
		(
			roll_pending_checks,
			render_checks,
			enforce_check_boosting_count,
			clear_pledges_on_result_change,
			check_submissions,
		));
		app.add_plugins(ContestedCheckPlugin);
	}
}

#[derive(Reflect, Clone, Component, Debug)]
pub struct Check
{
	pub dice: Option<Vec<DieIntegral>>,
	pub phase: CheckPhase,
	pub target_dice_count: usize,
	pub modifier: DieIntegral,
	pub success_threshhold: Option<DieIntegral>,
	pub keep_highest: bool,
	pub keep_amt: usize,
	pub responsible_players: Vec<Entity>,
}
impl Default for Check
{
	fn default() -> Self
	{
		Self
		{
			dice: default(),
			phase: default(),
			target_dice_count: 2,
			modifier: default(),
			success_threshhold: default(),
			keep_amt: 2,
			keep_highest: true,
			responsible_players: default(),
		}
	}
}
impl Check
{
	pub fn roll(&mut self) -> &mut Self
	{
		let mut rng = rand::thread_rng();
		self.dice = Some((0..self.target_dice_count).map(|_| rng.gen_range(DIE_MINIMUM..=DIE_MAXIMUM)).collect());
		self.phase = CheckPhase::NotBoosting;
		self
	}

	pub fn dice_picks(&self) -> impl Iterator<Item = (DieIntegral, bool)>
	{
		if self.dice.is_none() {return Default::default();}
		let dice = self.dice.as_ref().unwrap();
		let keep_amt = self.keep_amt.min(dice.len());
		if keep_amt == 0 {return Default::default();}
		let keep_ordering = if self.keep_highest {Ordering::Greater} else {Ordering::Less};
		let mut kept_amt: usize = 0;
		let mut kept: Vec<_> = dice.iter().map(|&d| (d, false)).collect();
		dice.iter().enumerate().for_each(|(i, die)|
		{
			if kept_amt < keep_amt
			{
				kept[i].1 = true;
				kept_amt += 1;
			}
			else
			{
				let worst_kept = kept.iter_mut()
					.filter(|d| d.1)
					.reduce(|p, n|
						if p.0.cmp(&n.0) == keep_ordering {n}
						else {p}
					)
					.unwrap();
				if die.cmp(&worst_kept.0) == keep_ordering
				{
					worst_kept.1 = false;
					kept[i].1 = true;
				}
			}
		});
		kept.into_iter()
	}

	pub fn kept_dice(&self) -> impl Iterator<Item = DieIntegral>
	{
		self.dice_picks().filter_map(|d| if d.1 {Some(d.0)} else {None})
	}

	pub fn total(&self) -> DieIntegral
	{
		self.kept_dice()
			.reduce(|t, d| t + d)
			.unwrap_or(0)
			+ self.modifier
	}

	pub fn predict_success(&self, pledge_bonus: AllocationIntegral) -> bool
	{
		let threshhold = self.success_threshhold.unwrap_or(0);
		self.total() + pledge_bonus as DieIntegral >= threshhold
	}

	pub fn is_success(&self) -> bool
	{
		self.predict_success(0)
	}
}

#[derive(Bundle)]
pub struct PendingCheckBundle
{
	name: Name,
	check: Check,
	scope: ScenarioScope,
}
impl PendingCheckBundle
{
	pub fn new<N: Into<Name>>(name: N, check: Check) -> Self
	{
		Self
		{
			name: name.into(),
			check,
			scope: default(),
		}
	}
}

#[derive(PartialEq, Eq, Clone, Copy, Reflect, Default, Debug)]
pub enum CheckPhase
{
	#[default]
	Pending,
	NotBoosting,
	Boosting,
	Submitted,
}

fn bracketed_dice_list<D: IntoIterator<Item = DieIntegral>>(dice: D) -> String
{
	format!
	(
		"[{}]",
		dice.into_iter().fold(String::new(), |acc, n|
		{
			match acc.is_empty()
			{
				true => format!("{}", n),
				false => format!("{},{}", acc, n),
			}
		})
	)
}

pub fn check_result_explanation(check: &Check) -> String
{
	let mut check = check.clone();
	if check.dice.is_none() {check.roll();}
	let dice = check.dice.as_ref().unwrap();
	format!
	(
		"{}{} + {} = {}{}",
		bracketed_dice_list(dice.to_owned()),
		{
			let kept: Vec<_> = check.kept_dice().collect();
			if check.keep_amt < dice.len()
			{
				format!
				(
					" Keep {} => {}",
					check.keep_amt,
					bracketed_dice_list(kept),
				)
			}
			else {default()}
		},
		check.modifier,
		check.total(),
		check.success_threshhold.map(|t| format!(" vs {}", t)).unwrap_or(default()),
	)
}

pub fn roll_pending_checks
(
	mut checks: Query<&mut Check>,
)
{
	for mut check in checks.iter_mut()
		.filter(|check| check.dice.is_none())
	{
		check.roll();
	}
}

pub fn render_checks
(
	mut egui: bevy_egui::EguiContexts,
	mut checks: Query
	<(
		Entity,
		Option<&Parent>,
		&Name,
		&mut Check,
	)>,
	containers: Query
	<
		(Entity, &Name, &Children),
		With<ContestedCheck>,
	>,
	mut power_pools: Query<(&mut PowerPools, &Actor)>,
	local_player: Query<Entity, With<IsLocalPlayer>>,
	mut submissions: EventWriter<SubmitCheckEvent>,
)
{
	let local_player = local_player.single();

	let mut owned_pools = power_pools.iter_mut()
		.filter_map(|(pools, actor)|
		{
			if actor.controller == local_player {Some(pools)}
			else {None}
		})
		.collect::<Vec<_>>();
	let mut pledges = Vec::<_>::new();
	for pools in owned_pools.iter_mut()
	{
		for pledged in pools.get_pledged() {pledges.push(pledged);}
	}
	let pledge_values = pledges.iter()
		.map(|allocation| allocation.value)
		.collect::<Vec<_>>();
	let mut pledge_toggles = std::iter::repeat(false)
		.take(pledges.len())
		.collect::<Vec<_>>();

	let checks_by_container = containers.iter()	
		.map(|(entity, name, children)| (entity, name.clone(), children.iter().map(|&e| e).collect::<Vec<_>>().into_iter()))
		.chain
		(
			checks.iter()
				.filter_map(|(entity, parent, name, ..)|
					if parent.is_none() {Some((entity, name))}
					else {None}
				)
				.map(|(entity, name)|
				(
					entity,
					name.clone(),
					vec![entity].into_iter(),
				))
		)
		.collect::<Vec<_>>();

	let ctx = egui.ctx_mut();
	let default_pos = ctx.available_rect().center_top();
	for entry in checks_by_container.into_iter()
	{
		let (container, container_name, container_children) = entry;
		egui::Window::new(container_name.to_string())
			.default_pos(default_pos)
			.pivot(egui::Align2::CENTER_TOP)
			.auto_sized()
			.per_entity(container)
			.show(ctx, |ui|
			{
				for child in container_children
				{
					if let Some((check_entity, _, name, mut check)) = checks.get_mut(child).ok()
					{
						let mut widget = CheckDisplayWidget::new(name, &check);
						let is_responsible = check.responsible_players.iter().any(|p| *p == local_player);
						let initial_phase = check.phase;
						let mut selected_phase = initial_phase;
						if is_responsible
						{
							widget = widget.with_boost_and_submit_button(&mut selected_phase);
							if initial_phase == CheckPhase::Boosting
							{
								widget = widget.with_pledges(pledge_values.as_slice(), pledge_toggles.as_mut_slice());
							}
						}
						widget.ui(ui);
						if selected_phase != initial_phase
						{
							if selected_phase == CheckPhase::Submitted
							{
								submissions.send(SubmitCheckEvent
								{
									check: check_entity,
									player: local_player,
								});
							}
							else {check.phase = selected_phase;}
						}
						else if initial_phase == CheckPhase::Boosting
						{
							pledge_toggles.iter().zip(pledges.iter_mut())
								.for_each(|(toggle, allocation)|
								{
									if *toggle {allocation.is_pledged = !allocation.is_pledged;}
								});
						}
					}
				}
			});
	}
}

#[derive(Event, Reflect)]
pub struct SubmitCheckEvent
{
	pub player: Entity,
	pub check: Entity,
}
pub fn check_submissions
(
	mut submissions: EventReader<SubmitCheckEvent>,
	mut pools: Query<(&mut PowerPools, &Actor)>,
	mut checks: Query<&mut Check>,
)
{
	for submission in submissions.read()
	{
		if let Some(mut check) = checks.get_mut(submission.check).ok()
		{
			let pledge_total = pools.iter_mut()
				.filter(|(_, actor)| actor.controller == submission.player)
				.map(|(mut pools, ..)|
					pools.get_pledged().fold(0, |sum, allocation|
					{
						let retval = allocation.value;
						allocation.is_pledged = false;
						allocation.value = 0;
						sum + retval
					})
				)
				.sum::<AllocationIntegral>();
			check.modifier += pledge_total as DieIntegral;
			check.phase = CheckPhase::Submitted;
		}
	}
}

pub fn enforce_check_boosting_count
(
	mut check_results: Query<&mut Check>,
	local_player: Query<Entity, With<IsLocalPlayer>>
)
{
	let local_player = local_player.single();
	let mut first_non_boosting_check = Option::<_>::default();
	let mut boosting_checks = check_results.iter_mut()
		.filter(|check|
			check.responsible_players.iter().any(|&p| p == local_player)
		)
		.filter_map(|check|
		{
			if check.phase == CheckPhase::Boosting {Some(check)}
			else
			{
				if check.phase == CheckPhase::NotBoosting && first_non_boosting_check.is_none()
				{
					first_non_boosting_check = Some(check);
				}
				None
			}
		})
		.collect::<Vec<_>>();
	if boosting_checks.len() == 0
	{
		if let Some(mut check) = first_non_boosting_check
		{
			check.phase = CheckPhase::Boosting;
		}
	}
	else if boosting_checks.len() > 1
	{
		boosting_checks.iter_mut()
			.filter(|result| !result.is_changed())
			.for_each(|result|
			{
				result.phase = CheckPhase::NotBoosting;
			});
	}
}

pub fn clear_pledges_on_result_change
(
	mut power_pools: Query<&mut PowerPools>,
	changed_results: Query<(), Changed<Check>>,
)
{
	if !changed_results.is_empty()
	{
		for mut power_pool in power_pools.iter_mut()
		{
			power_pool.clear_pledges();
		}
	}
}