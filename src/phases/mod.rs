use bevy::prelude::*;

pub mod initiative;
pub use initiative::*;
pub mod planning;
pub use planning::*;
pub mod action;
pub use action::*;

pub struct PhasesPlugin;
impl Plugin for PhasesPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<GamePhase>();
		app.init_state::<GamePhase>();
		app.add_plugins(InitiativePlugin);
		app.add_plugins(PlanningPlugin);
		app.add_plugins(ActionPlugin);
	}
}

#[derive(States, Debug, Default, Clone, PartialEq, Eq, Hash, Reflect)]
pub enum GamePhase
{
	#[default]
	STAGING,
	INITIATIVE,
	PLANNING,
	ACTION,
}
impl std::fmt::Display for GamePhase
{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result
	{
		std::fmt::Debug::fmt(self, f)
    }
}
