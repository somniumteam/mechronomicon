use bevy::prelude::*;
use itertools::Itertools;
use std::cmp::Ordering;
use crate::*;

pub struct ActionPlugin;
impl Plugin for ActionPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<ResolutionPhase>();
		app.register_type::<ActionExecutionPlan>();
		app.register_type::<PathfindingInterpolation>();
		app.add_systems(OnEnter(GamePhase::ACTION), generate_execution_plan);
		app.add_systems(
			Update,
			(
				execute_plan_step,
			).run_if(in_state(GamePhase::ACTION)),
		);
	}
}

#[derive(Reflect, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub enum ResolutionPhase
{
	SELF,
	ALLIES,
	ENEMIES,
	FINALIZE,
}

#[derive(Resource, Default, Clone, Reflect)]
pub struct ActionExecutionPlan
{
	pub plan: Vec<(Entity, ActorCommand)>,
}

pub fn generate_execution_plan
(
	mut commands: Commands,
	mut actors: Query<(Entity, &mut Actor)>,
	initiative: Res<InitiativeOrder>,
)
{
	let initiative = &initiative.0;
	let mut plan = Vec::<_>::new();
	for (entity, mut actor) in actors.iter_mut()
	{
		plan.extend
		(
			actor.command_queue.drain(..)
				.enumerate()
				.map(|(i, cmd)| (entity, cmd, i))
		);
	}
	let sorted = plan.into_iter()
		.sorted_by(|(a_ent, a_cmd, a_i), (b_ent, b_cmd, b_i)|
		{
			let a_phase = a_cmd.phase;
			let b_phase = b_cmd.phase;
			match a_phase.cmp(&b_phase)
			{
				Ordering::Equal =>
				{
					initiative.iter()
						.find_map(|faction|
						{
							faction.members.iter().find_map(|&member|
							{
								if member == *a_ent {Some(Ordering::Less)}
								else if member == *b_ent {Some(Ordering::Greater)}
								else {None}
							})
						})
						.unwrap_or(a_i.cmp(b_i))
				}
				cmp => cmp
			}
		})
		.map(|(entity, command, ..)| (entity, command));
	commands.insert_resource(ActionExecutionPlan
	{
		plan: sorted.collect(),
		..default()
	});
}

pub fn execute_plan_step
(
	world: &mut World,
)
{
	let phase = world.get_resource::<State<GamePhase>>().unwrap();
	if *phase != GamePhase::ACTION {return;}
	let steps = &world.get_resource::<ActionExecutionPlan>().unwrap().plan;
	if steps.is_empty()
	{
		world.get_resource_mut::<NextState<GamePhase>>().unwrap().set(GamePhase::INITIATIVE);
		world.remove_resource::<ActionExecutionPlan>();
		world.remove_resource::<InitiativeOrder>();
	}
	else
	{
		let (host, command) = steps.first().unwrap();
		if world.run_action_resolution(*host, command.command_type, command.params)
		{
			world.get_resource_mut::<ActionExecutionPlan>().unwrap().plan.remove(0);
		}
	}
}

#[derive(Component, Default, Reflect, Clone)]
pub struct PathfindingInterpolation
{
	pub remaining_positions: Vec<HexCoordinate>,
}