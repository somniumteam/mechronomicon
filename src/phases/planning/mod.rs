use bevy::prelude::*;
use crate::*;

pub mod commands;
pub use commands::*;

pub struct PlanningPlugin;
impl Plugin for PlanningPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<PlanningOrderIndex>();
		app.register_type::<IsPlanning>();
		app.register_type::<SubmittedPlan>();
		app.add_event::<SubmitPlanEvent>();
		app.add_plugins(PlanCommandsPlugin);
		app.add_systems(OnEnter(GamePhase::PLANNING), populate_initial_planning_order);
		app.add_systems(OnExit(GamePhase::PLANNING), cleanup_planning_order);
		app.add_systems(
			Update,
			(
				update_is_planning,
				advance_plan_index,
				plan_submissions,
			).run_if(in_state(GamePhase::PLANNING))
		);
	}
}

#[derive(Resource, Reflect, Default)]
pub struct PlanningOrderIndex(pub usize);

#[derive(Component, Reflect, Default)]
pub struct IsPlanning;

pub fn populate_initial_planning_order(mut commands: Commands) {commands.init_resource::<PlanningOrderIndex>();}
pub fn cleanup_planning_order(mut commands: Commands) {commands.remove_resource::<PlanningOrderIndex>();}

pub fn update_is_planning
(
	mut commands: Commands,
	players: Query<Option<&IsPlanning>, With<Player>>,
	initiative: Res<InitiativeOrder>,
	order_index: Res<PlanningOrderIndex>,
)
{
	for (i, faction) in initiative.0.iter().enumerate()
	{
		let should_plan = i == order_index.0;
		for &member in faction.members.iter()
		{
			let is_planning = players.get(member)
				.map(|p| p.is_some())
				.unwrap();
			if is_planning != should_plan
			{
				match should_plan
				{
					true => {commands.entity(member).insert(IsPlanning);},
					false => {commands.entity(member).remove::<IsPlanning>();},
				}
			}
		}
	}
}

#[derive(Component, Default, Reflect)]
pub struct SubmittedPlan;

pub fn advance_plan_index
(
	mut commands: Commands,
	players: Query<Option<&SubmittedPlan>, With<Player>>,
	initiative: Res<InitiativeOrder>,
	mut order_index: ResMut<PlanningOrderIndex>,
	mut next_phase: ResMut<NextState<GamePhase>>,
)
{
	if let Some(faction) = initiative.0.get(order_index.0)
	{
		let all_submitted = faction.members.iter().all(|&member|
			players.get(member)
				.map(|p| p.is_some())
				.unwrap()
		);
		if all_submitted
		{
			faction.members.iter().for_each(|&member|
			{
				commands.entity(member).remove::<SubmittedPlan>();
			});
			order_index.0 += 1;
		}
	}
	else
	{
		next_phase.set(GamePhase::ACTION);
	}
}

#[derive(Event, Reflect)]
pub struct SubmitPlanEvent
{
	pub player: Entity,
}

pub fn plan_submissions
(
	mut commands: Commands,
	mut submissions: EventReader<SubmitPlanEvent>,
)
{
	for submission in submissions.read()
	{
		commands.entity(submission.player).insert(SubmittedPlan);
	}
}