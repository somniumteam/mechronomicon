use bevy::prelude::*;
use crate::*;

pub struct PlanCommandsPlugin;
impl Plugin for PlanCommandsPlugin
{
	fn build(&self, app: &mut App)
	{
		app.add_systems(Update,
		(
			issue_command
				.run_if(in_state(GamePhase::PLANNING))
				.run_if(not(resource_exists::<IsEguiCapturingMouse>)),
		));
	}
}

pub fn issue_command
(
	mut commands: Commands,
    clicks: Res<ButtonInput<MouseButton>>,
	mut selected: Query
	<
		(
			Entity,
			&mut Actor,
			&HexCoordinate,
		),
		With<IsSelected>,
	>,
	cursor: Res<CursorPosition>,
	terrain: Option<Res<TerrainState>>,
	is_planning: Query
	<
		(),
		(
			With<IsLocalPlayer>,
			With<IsPlanning>,
			Without<SubmittedPlan>,
		)
	>,
)
{
	if is_planning.get_single().is_err() {return;}
	if clicks.just_pressed(MouseButton::Right)
	{
		if let Some(hex) = cursor.hex
		{
			if let Some((selected, mut actor, &selected_hex)) = selected.get_single_mut().ok()
			{
				let mut entity_commands = commands.entity(selected);
				if selected_hex == hex
				{
					entity_commands.remove::<PathfindingRequest>();
					if !actor.command_queue.is_empty() {actor.command_queue.clear();}
				}
				else if let Some(terrain) = terrain
				{
					if let Some((terrain, _)) = terrain.get(hex)
					{
						if terrain.base_type != "blank"
						{
							let new_command = ActorCommand::new
							(
								ActorCommandType::Move,
								ActorCommandParams::Hex { hex },
							);
							let queue = &mut actor.command_queue;
							if !queue.is_empty() {queue.pop();}
							queue.push(new_command);
							entity_commands.insert(PathfindingRequest::new(selected_hex, hex));
						}
					}
				}
			}
		}
	}
}