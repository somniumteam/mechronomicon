use bevy::prelude::*;
use crate::*;

pub struct InitiativePlugin;
impl Plugin for InitiativePlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<InitiativeCheck>();
		app.register_type::<InitiativeOrder>();
		app.add_systems(OnEnter(GamePhase::INITIATIVE), launch_initiative_checks);
		app.add_systems(Update,
			conclude_initiative_phase.run_if(in_state(GamePhase::INITIATIVE))
		);
	}
}

#[derive(Component, Reflect, Default)]
pub struct InitiativeCheck;

pub fn launch_initiative_checks
(
	mut commands: Commands,
	players: Query<(Entity, &Parent), With<Player>>,
	factions: Query<(Entity, &Name), (With<Children>, With<Faction>)>,
)
{
	commands.remove_resource::<InitiativeOrder>();
	let players_by_faction_name = factions.iter()
		.map(|(faction, faction_name)|
		{
			let players_in_faction = players.iter()
				.filter(|(_, p)| p.get() == faction)
				.map(|(e, ..)| e)
				.collect::<Vec<_>>()
				.into_iter();
			(faction_name.to_owned(), players_in_faction)
		});
	commands
		.create_contested_check
		(
			"Initiative Check",
			&Check::default(),
			players_by_faction_name,
		)
		.insert(InitiativeCheck);
}

pub fn conclude_initiative_phase
(
	mut commands: Commands,
	initiative: Query<(Entity, &ContestedCheck, &Children), With<InitiativeCheck>>,
	mut advance_phase: ResMut<NextState<GamePhase>>,
)
{
	if let Some((container, check, children)) = initiative.get_single().ok()
	{
		if check.competing_factions.is_empty()
		{
			children.iter().for_each(|&c| {commands.retire_check(c);});
			commands.entity(container).despawn_recursive();
			commands.insert_resource(InitiativeOrder(check.finished_factions.to_owned()));
			advance_phase.set(GamePhase::PLANNING);
		}
	}
}

#[derive(Resource, Reflect)]
pub struct InitiativeOrder(pub Vec<ContestFactionInfo>);