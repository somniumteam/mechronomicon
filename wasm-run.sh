#!/bin/bash

cd "$(dirname "$(readlink -f "$0")")"
pwd

export RUSTFLAGS="--cfg=web_sys_unstable_apis"
export WASM_SERVER_RUNNER_CUSTOM_INDEX_HTML="assets/index.html"
cargo run --target=wasm32-unknown-unknown
