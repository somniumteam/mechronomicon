#!/bin/bash

cd "$(dirname "$(readlink -f "$0")")"

OUT_DIR="$PWD/target/deploy-wasm"

if [[ -d "$OUT_DIR" ]]
then
	rm -r "$OUT_DIR"/*
else
	mkdir -p "$OUT_DIR"
fi

"$HOME/.cargo/bin/wasm-bindgen" \
    --no-typescript --target web \
    --out-dir "$OUT_DIR" \
    --out-name "mechronomicon" \
    ./target/wasm32-unknown-unknown/debug/mechronomicon.wasm ||
	exit $?

cp -r assets "$OUT_DIR/assets" || exit $?
sed -e's|// {{ MODULE }}|import wasm_bindgen from "./mechronomicon.js";|' \
	-e's|\(let bindgen_param = \).*;|\1undefined;|' \
	"$OUT_DIR/assets/index.html" \
	> "$OUT_DIR/index.html" \
	|| exit $?

cd "$OUT_DIR" || exit $?
7z a mechronomicon-wasm.zip ./*
exit $?